package com.raul.PeliculasySeries.Variables;

/**
 *
 * @author Raul
 *
 */
public class Constantes {

	// SQL SERVER VARIABLES DE CONEXION
	/**
	 *
	 */
	// public final static String URLSERVIDOR =
	// "jdbc:sqlserver://BMW\\SQLSERVER:1433;databaseName=PeliculasSeries";
	/**
	 *
	 */
	// public final static String USER = "sa";
	/**
	 *
	 */
	// public final static String PASSWORD = "sa";
	/**
	 *
	 */
	public final static String DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";

	/**
	 *
	 */
	public final static String RUTASERVIDORPDF = System
			.getProperty("catalina.base") + "\\webapps\\PeliculasySeries\\";

	/**
	 *
	 */
	public final static String RUTASERVIDORCONF = System
			.getProperty("catalina.base") + "\\conf\\PeliculasySeries\\";

	/**
	 *
	 */
	public final static String RUTAINFORMES = System
			.getProperty("catalina.base") + "\\conf\\PeliculasySeries\\";
}
