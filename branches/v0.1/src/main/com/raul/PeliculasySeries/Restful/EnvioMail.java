package com.raul.PeliculasySeries.Restful;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.raul.PeliculasySeries.Beans.EnvioMailBean;
import com.raul.PeliculasySeries.Variables.Constantes;

@Path("/envioemail")
public class EnvioMail {

	private static String RUTAPROPIEDADES = Constantes.RUTASERVIDORCONF
			+ "\\mail.properties";

	InternetAddress direccion = null;

	private Session configuracion() throws FileNotFoundException, IOException {
		final Properties props = getProperties();

		Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(props
								.getProperty("mail.user"), props
								.getProperty("mail.password"));
					}
				});

		direccion = new InternetAddress(props.getProperty("mail.email.sender"),
				props.getProperty("mail.name.sender"));
		return session;
	}

	@POST
	@Path("")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response sendEmail(EnvioMailBean enviarMail) {
		try {

			Session session = configuracion();

			Message message = new MimeMessage(session);
			message.setFrom(direccion);

			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(enviarMail.getEmail()));

			message.setSubject("Envio de informe");
			// message.setText(enviarMail.getMensaje());

			// archivo adunto
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setText(enviarMail.getMensaje());

			BodyPart attachment = new MimeBodyPart();

			// Part two is attachment
			// messageBodyPart = new MimeBodyPart();
			DataSource source = new FileDataSource(
					enviarMail.getArchivoEnviar());
			attachment.setDataHandler(new DataHandler(source));
			attachment.setFileName(enviarMail.getFileName());

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			multipart.addBodyPart(attachment);

			message.setContent(multipart);

			Transport.send(message);

		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		} catch (MessagingException e) {

			e.printStackTrace();
		}

		return Response.status(200).entity("Mensaje enviado").build();
	}

	private Properties getProperties() throws FileNotFoundException,
			IOException {
		Properties res = new Properties();
		InputStream input = null;

		input = new FileInputStream(RUTAPROPIEDADES);

		res.load(input);

		return res;
	}
}
