package com.raul.PeliculasySeries.Restful;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;

import com.raul.PeliculasySeries.Informes.DataBeans.DataBeanListProductoras;
import com.raul.PeliculasySeries.Variables.Constantes;
import com.raul.PeliculasySeries.Variables.VariablesComunes;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

@Path("/informes")
@Api(value = "/informes", description = "Controlador para obtener los informes de la aplicacion.")
public class Informes {

	@GET
	@Path("")
	@ApiOperation(value = "Realiza una consulta a la Base de datos por la cual obtenemos el listado completo de las peliculas y de las series.", notes = "Informes", produces = "Genera un JSON con el resultado")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Consulta realizada correctamente con datos"),

			@ApiResponse(code = 204, message = "Consulta realizada correctamente pero no ha devuelto datos"),

			@ApiResponse(code = 500, message = "Error en la conexion a los datos o a la hora de montar el JSON") })
	public static Response getInformeTotal() {
		Connection cnn = null;
		JasperReport jasperReport;
		JasperPrint jasperPrint;
		// HashMap jasperParameter = new HashMap();
		JRExporter exporter = new JRPdfExporter();
		FileOutputStream exportado = null;

		try {
			Class.forName(Constantes.DRIVER);
			cnn = DriverManager.getConnection(VariablesComunes.urlConexion,
					VariablesComunes.user, VariablesComunes.pass);
			cnn.setAutoCommit(false);

			jasperReport = JasperCompileManager
					.compileReport("D:\\Programacion\\Programacion Java\\PeliculasySeries\\Informes\\InformeGeneral.jrxml");

			jasperPrint = JasperFillManager.fillReport(jasperReport, null, cnn);

			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exportado = new FileOutputStream(Constantes.RUTASERVIDORPDF
					+ "pdf//informegeneral.pdf");

			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, exportado);

			exporter.exportReport();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JRException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				cnn.close();
				exportado.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		return Response.status(200).entity("../pdf/informegeneral.pdf").build();
	}

	@GET
	@Path("/portipo")
	@ApiOperation(value = "Realiza una consulta a la Base de datos por la cual obtenemos el listado completo de las peliculas y de las series.", notes = "Informes", produces = "Genera un JSON con el resultado")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Consulta realizada correctamente con datos"),

			@ApiResponse(code = 204, message = "Consulta realizada correctamente pero no ha devuelto datos"),

			@ApiResponse(code = 500, message = "Error en la conexion a los datos o a la hora de montar el JSON") })
	public Response getInformePorTipo() {
		Connection cnn = null;
		JasperReport jasperReport;
		JasperPrint jasperPrint;
		// HashMap jasperParameter = new HashMap();
		JRExporter exporter = new JRPdfExporter();
		FileOutputStream exportado = null;

		try {
			Class.forName(Constantes.DRIVER);
			cnn = DriverManager.getConnection(VariablesComunes.urlConexion,
					VariablesComunes.user, VariablesComunes.pass);
			cnn.setAutoCommit(false);

			jasperReport = JasperCompileManager
					.compileReport("D:\\Programacion\\Programacion Java\\PeliculasySeries\\Informes\\InformeCompletoPorTipo.jrxml");

			jasperPrint = JasperFillManager.fillReport(jasperReport, null, cnn);

			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exportado = new FileOutputStream(Constantes.RUTASERVIDORPDF
					+ "pdf//informecompletoportipo.pdf");

			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, exportado);

			exporter.exportReport();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JRException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				cnn.close();
				exportado.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		return Response.status(200).entity("../pdf/informecompletoportipo.pdf")
				.build();

	}

	@GET
	@Path("/porproductora")
	@ApiOperation(value = "Realiza una consulta a la Base de datos por la cual obtenemos el listado completo de las peliculas y de las series.", notes = "Informes", produces = "Genera un JSON con el resultado")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Consulta realizada correctamente con datos"),

			@ApiResponse(code = 204, message = "Consulta realizada correctamente pero no ha devuelto datos"),

			@ApiResponse(code = 500, message = "Error en la conexion a los datos o a la hora de montar el JSON") })
	public Response getInformePorProductora() {
		Connection cnn = null;
		JasperReport jasperReport;
		JasperPrint jasperPrint;
		// HashMap jasperParameter = new HashMap();
		JRExporter exporter = new JRPdfExporter();
		FileOutputStream exportado = null;

		try {
			Class.forName(Constantes.DRIVER);
			cnn = DriverManager.getConnection(VariablesComunes.urlConexion,
					VariablesComunes.user, VariablesComunes.pass);
			cnn.setAutoCommit(false);

			jasperReport = JasperCompileManager
					.compileReport("D:\\Programacion\\Programacion Java\\PeliculasySeries\\Informes\\InformeCompletoPorProductoras.jrxml");

			jasperPrint = JasperFillManager.fillReport(jasperReport, null, cnn);

			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exportado = new FileOutputStream(Constantes.RUTASERVIDORPDF
					+ "pdf//informecompletoporproductora.pdf");

			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, exportado);

			exporter.exportReport();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JRException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				cnn.close();
				exportado.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		return Response.status(200)
				.entity("../pdf/informecompletoporproductora.pdf").build();

	}

	@GET
	@Path("/estadistica/porproductora")
	@ApiOperation(value = "Realiza una consulta a la Base de datos por la cual obtenemos el listado completo de las peliculas y de las series.", notes = "Informes", produces = "Genera un JSON con el resultado")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Consulta realizada correctamente con datos"),

			@ApiResponse(code = 204, message = "Consulta realizada correctamente pero no ha devuelto datos"),

			@ApiResponse(code = 500, message = "Error en la conexion a los datos o a la hora de montar el JSON") })
	public static Response getInformeEstadisticaPorProductora() {
		Connection cnn = null;
		JasperReport jasperReport;
		JasperPrint jasperPrint;
		// HashMap jasperParameter = new HashMap();
		// JRExporter exporter = new JRPdfExporter();
		// FileOutputStream exportado = null;

		try {
			DataBeanListProductoras dataBeanListProductoras = new DataBeanListProductoras();
			String sourceFileName = "D:\\Programacion\\Programacion Java\\PeliculasySeries\\Informes\\EstadisticaPorProductora.jrxml";
			jasperReport = JasperCompileManager.compileReport(sourceFileName);
			jasperPrint = JasperFillManager.fillReport(
					jasperReport,
					null,
					new JRBeanCollectionDataSource(dataBeanListProductoras
							.getDataBeanList()));
			JasperExportManager.exportReportToPdfFile(jasperPrint, ".pdf");
			/*
			 * Class.forName(Constantes.DRIVER); cnn =
			 * DriverManager.getConnection(Constantes.URLSERVIDOR,
			 * Constantes.USER, Constantes.PASSWORD); cnn.setAutoCommit(false);
			 */

			/*
			 * DataBeanListProductoras dataBeanListProductoras = new
			 * DataBeanListProductoras(); ArrayList<EstadisticasProductora>
			 * dataList = dataBeanListProductoras .getDataBeanList();
			 * 
			 * JRBeanCollectionDataSource beanColDataSource = new
			 * JRBeanCollectionDataSource( dataList);
			 * 
			 * jasperReport = JasperCompileManager .compileReport(
			 * "D:\\Programacion\\Programacion Java\\PeliculasySeries\\Informes\\EstadisticaPorProductora.jrxml"
			 * );
			 * 
			 * Map parameters = new HashMap();
			 * 
			 * jasperPrint = JasperFillManager.fillReport(jasperReport,
			 * parameters, beanColDataSource);
			 * 
			 * exporter.setParameter(JRExporterParameter.JASPER_PRINT,
			 * jasperPrint); exportado = new
			 * FileOutputStream(Constantes.RUTASERVIDORPDF +
			 * "pdf//EstadisticaPorProductora.pdf");
			 * 
			 * exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
			 * exportado);
			 * 
			 * exporter.exportReport();
			 */
		} catch (JRException e) {
			e.printStackTrace();
			/*
			 * } catch (FileNotFoundException e) { e.printStackTrace();
			 */
		} finally {
			/*
			 * try { cnn.close(); if (exportado != null) { exportado.close(); }
			 * } catch (SQLException e) { e.printStackTrace(); } catch
			 * (IOException e) { e.printStackTrace(); }
			 */

		}

		return Response.status(200)
				.entity("../pdf/EstadisticaPorProductora.pdf").build();

	}

	public static void main(String[] args) {
		getInformeEstadisticaPorProductora();
	}
}
