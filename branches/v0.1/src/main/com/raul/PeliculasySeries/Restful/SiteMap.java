package com.raul.PeliculasySeries.Restful;

import java.io.File;
import java.net.MalformedURLException;
import java.util.Date;

import com.redfin.sitemapgenerator.ChangeFreq;
import com.redfin.sitemapgenerator.WebSitemapGenerator;
import com.redfin.sitemapgenerator.WebSitemapUrl;

public class SiteMap {

	public static void main(String[] args) {
		File myDir = new File("c:\\mi mapa");

		try {
			WebSitemapGenerator wsg = new WebSitemapGenerator(
					"http://localhost:8080/PeliculasySeries/", myDir);
			WebSitemapUrl url = new WebSitemapUrl.Options(
					"http://localhost:8080/PeliculasySeries/index.html")
					.lastMod(new Date()).priority(1.0)
					.changeFreq(ChangeFreq.HOURLY).build();
			// this will configure the URL with lastmod=now, priority=1.0,
			// changefreq=hourly
			wsg.addUrl(url);
			wsg.write();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

	}
}
