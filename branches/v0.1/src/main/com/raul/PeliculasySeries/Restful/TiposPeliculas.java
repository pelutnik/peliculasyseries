package com.raul.PeliculasySeries.Restful;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;

import com.raul.PeliculasySeries.Beans.MensajesBean;
import com.raul.PeliculasySeries.Beans.TiposPeliculasBean;
import com.raul.PeliculasySeries.Database.Factoria.DAOFactory;
import com.raul.PeliculasySeries.Database.interfaces.InterfaceCategoriasDAO;
import com.raul.PeliculasySeries.Variables.Constantes;
import com.raul.PeliculasySeries.Variables.VariablesComunes;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

@Path("/tipospeliculas")
// @Api(value = "/pelisseries", description =
// "Controlador para obtener el recurso matriculas")
public class TiposPeliculas {

	private static DAOFactory fSQLServer;
	private static InterfaceCategoriasDAO categoriasDao;

	/**
	 * 
	 */
	public TiposPeliculas() {
		fSQLServer = DAOFactory.getDAOFactory(DAOFactory.SQLSERVER);
		categoriasDao = fSQLServer.getInterfaceCategoriasDAO();
	}

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Realiza una consulta a la Base de datos por la cual obtenemos el listado completo de las peliculas y de las series.", notes = "Peliculas y series", produces = "Genera un JSON con el resultado")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Consulta realizada correctamente con datos"),

			@ApiResponse(code = 204, message = "Consulta realizada correctamente pero no ha devuelto datos"),

			@ApiResponse(code = 500, message = "Error en la conexion a los datos o a la hora de montar el JSON") })
	public final Response getTiposPeliculas() {
		// Aqui va el codigo de llamada a la BD para que nos devuelva toda la
		// lista de peliculas y de series
		// PeliculasSeries pelis = new PeliculasSeries();
		ArrayList<TiposPeliculasBean> listaTipos = new ArrayList<TiposPeliculasBean>();
		Response respuesta = null;

		try {
			listaTipos = categoriasDao.obtenerDatos();
			respuesta = Response.status(200).entity(listaTipos).build();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return respuesta;
	}

	@GET
	@Path("/detalle")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Realiza una consulta a la Base de datos por la cual obtenemos el listado completo de las peliculas y de las series.", notes = "Peliculas y series", produces = "Genera un JSON con el resultado")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Consulta realizada correctamente con datos"),

			@ApiResponse(code = 204, message = "Consulta realizada correctamente pero no ha devuelto datos"),

			@ApiResponse(code = 500, message = "Error en la conexion a los datos o a la hora de montar el JSON") })
	public final Response getUnTiposPelicula(@QueryParam(value = "id") int id) {

		TiposPeliculasBean categoria = null;
		Response respuesta = null;

		try {
			categoria = categoriasDao.consultaUnTipoPelicula(id);
			respuesta = Response.status(200).entity(categoria).build();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return respuesta;
	}

	@POST
	@Path("/insertar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Realiza una consulta a la Base de datos por la cual obtenemos el listado completo de las peliculas y de las series.", notes = "Peliculas y series", produces = "Genera un JSON con el resultado")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Consulta realizada correctamente con datos"),

			@ApiResponse(code = 204, message = "Consulta realizada correctamente pero no ha devuelto datos"),

			@ApiResponse(code = 500, message = "Error en la conexion a los datos o a la hora de montar el JSON") })
	public Response addTipos(TiposPeliculasBean tipo) {
		MensajesBean mensaje = new MensajesBean();

		try {
			categoriasDao.insertarTipo(tipo);
			mensaje.setMensaje("OK");
			mensaje.setCodigoError(200);
		} catch (ClassNotFoundException e) {
			mensaje.setMensaje(e.getMessage());
			mensaje.setCodigoError(500);
		} catch (SQLException e) {
			mensaje.setMensaje(e.getMessage());
			mensaje.setCodigoError(500);
		}

		return Response.status(mensaje.getCodigoError()).entity(mensaje)
				.build();
	}

	@PUT
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCategorias(TiposPeliculasBean tipo,
			@QueryParam(value = "id") int id) {

		MensajesBean mensaje = new MensajesBean();

		try {
			categoriasDao.actualizarRegistro(tipo, id);
			mensaje.setMensaje("OK");
			mensaje.setCodigoError(200);
		} catch (ClassNotFoundException e) {
			mensaje.setMensaje(e.getMessage());
			mensaje.setCodigoError(500);
		} catch (SQLException e) {
			mensaje.setMensaje(e.getMessage());
			mensaje.setCodigoError(500);
		}

		return Response.status(mensaje.getCodigoError()).entity(mensaje)
				.build();
	}

	@DELETE
	@Path("/borrar")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteRecord(@QueryParam(value = "id") int id) {
		MensajesBean mensaje = new MensajesBean();

		try {
			categoriasDao.borrarRegistro(id);
			mensaje.setMensaje("OK");
			mensaje.setCodigoError(200);
		} catch (ClassNotFoundException e) {
			mensaje.setMensaje(e.getMessage());
			mensaje.setCodigoError(500);
		} catch (SQLException e) {
			mensaje.setMensaje(e.getMessage());
			mensaje.setCodigoError(500);
		}

		return Response.status(mensaje.getCodigoError()).entity(mensaje)
				.build();
	}

	@GET
	@Path("/informe")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Realiza una consulta a la Base de datos por la cual obtenemos el listado completo de las peliculas y de las series.", notes = "Informes", produces = "Genera un JSON con el resultado")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Consulta realizada correctamente con datos"),

			@ApiResponse(code = 204, message = "Consulta realizada correctamente pero no ha devuelto datos"),

			@ApiResponse(code = 500, message = "Error en la conexion a los datos o a la hora de montar el JSON") })
	public Response getInformePorTipo() {
		JasperReport jasperReport;
		JasperPrint jasperPrint;
		JRExporter exporter = new JRPdfExporter();
		FileOutputStream exportado = null;
		MensajesBean mensaje = null;

		try {

			VariablesComunes.conexion.setAutoCommit(false);

			jasperReport = JasperCompileManager
					.compileReport("D:\\Programacion\\Programacion Java\\PeliculasySeries\\Informes\\InformeCompletoPorTipo.jrxml");

			jasperPrint = JasperFillManager.fillReport(jasperReport, null,
					VariablesComunes.conexion);

			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exportado = new FileOutputStream(Constantes.RUTASERVIDORPDF
					+ "pdf//informecompletoportipo.pdf");

			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, exportado);

			exporter.exportReport();

			mensaje = new MensajesBean();
			mensaje.setCodigoError(200);
			mensaje.setMensaje("../pdf/informecompletoportipo.pdf");
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JRException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				VariablesComunes.conexion.setAutoCommit(true);
				exportado.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		return Response.status(mensaje.getCodigoError()).entity(mensaje)
				.build();

	}
}
