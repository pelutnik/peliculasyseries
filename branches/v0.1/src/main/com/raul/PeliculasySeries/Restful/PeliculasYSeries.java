package com.raul.PeliculasySeries.Restful;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;

import org.slf4j.Logger;

import com.raul.PeliculasySeries.Beans.MensajesBean;
import com.raul.PeliculasySeries.Beans.PeliculasBean;
import com.raul.PeliculasySeries.Configs.LoggerConfig;
import com.raul.PeliculasySeries.Database.Factoria.DAOFactory;
import com.raul.PeliculasySeries.Database.interfaces.InterfacePeliculasDAO;
import com.raul.PeliculasySeries.Variables.Constantes;
import com.raul.PeliculasySeries.Variables.VariablesComunes;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

@Path("/pelisseries/")
// @Api(value = "/pelisseries", description =
// "Controlador para obtener el recurso matriculas")
@Api(value = "/pelisseries", description = "Controlador para obtener el recurso matriculas")
public class PeliculasYSeries {

	Logger log;
	private static DAOFactory fSQLServer;
	private static InterfacePeliculasDAO peliculasDao;

	public PeliculasYSeries() {
		super();

		if (log == null) {
			log = LoggerConfig.configurarLog("PeliculasYSeries");
		}

		fSQLServer = DAOFactory.getDAOFactory(DAOFactory.SQLSERVER);
		peliculasDao = fSQLServer.getInterfacePeliculasDAO();
	}

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Realiza una consulta a la Base de datos por la cual obtenemos el listado completo de las peliculas y de las series.", notes = "Peliculas y series", produces = "Genera un JSON con el resultado")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Consulta realizada correctamente con datos"),

			@ApiResponse(code = 204, message = "Consulta realizada correctamente pero no ha devuelto datos"),

			@ApiResponse(code = 500, message = "Error en la conexion a los datos o a la hora de montar el JSON") })
	public final Response getPelisYSeries() {
		// Aqui va el codigo de llamada a la BD para que nos devuelva toda la
		// lista de peliculas y de series
		// PeliculasSeries pelis = new PeliculasSeries();
		ArrayList<PeliculasBean> listaMultimedia = new ArrayList<PeliculasBean>();
		Response respuesta = null;
		MensajesBean mensaje = new MensajesBean();

		try {
			listaMultimedia = peliculasDao.obtenerDatos();
			respuesta = Response.status(200).entity(listaMultimedia).build();
			log.info("Peliculas y series cargadas");
		} catch (ClassNotFoundException e) {
			mensaje.setMensaje(e.getMessage());
			mensaje.setCodigoError(500);
			respuesta = Response.status(mensaje.getCodigoError())
					.entity(mensaje).build();

			log.error(e.getMessage());

		} catch (SQLException e) {
			mensaje.setMensaje(e.getMessage());
			mensaje.setCodigoError(500);
			respuesta = Response.status(mensaje.getCodigoError())
					.entity(mensaje).build();

			log.error(e.getMessage());
		}

		return respuesta;
	}

	@GET
	@Path("peliculaunica")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPeliculaOSerie(@QueryParam(value = "id") int id,
			@QueryParam(value = "edicion") boolean edicion) {
		PeliculasBean pelicula = null;
		MensajesBean mensaje = new MensajesBean();

		try {
			pelicula = peliculasDao.cargarPelicula(id, edicion);
			mensaje.setMensaje("ok");
			mensaje.setCodigoError(200);
		} catch (ClassNotFoundException e) {
			mensaje.setMensaje(e.getMessage());
			mensaje.setCodigoError(500);
		} catch (SQLException e) {
			mensaje.setMensaje(e.getMessage());
			mensaje.setCodigoError(500);
		}

		return Response.status(mensaje.getCodigoError()).entity(pelicula)
				.build();
	}

	@POST
	@Path("insertar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Realiza una consulta a la Base de datos por la cual obtenemos el listado completo de las peliculas y de las series.", notes = "Peliculas y series", produces = "Genera un JSON con el resultado")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Consulta realizada correctamente con datos"),

			@ApiResponse(code = 204, message = "Consulta realizada correctamente pero no ha devuelto datos"),

			@ApiResponse(code = 500, message = "Error en la conexion a los datos o a la hora de montar el JSON") })
	public final Response introducirMultimedia(PeliculasBean pelis) {
		MensajesBean mensaje = new MensajesBean();
		try {
			peliculasDao.insertarMultimedia(pelis);
			mensaje.setMensaje("ok");
			mensaje.setCodigoError(200);
		} catch (ClassNotFoundException e) {
			mensaje.setMensaje(e.getMessage());
			mensaje.setCodigoError(500);
		} catch (SQLException e) {
			mensaje.setMensaje(e.getMessage());
			mensaje.setCodigoError(500);
		}

		return Response.status(mensaje.getCodigoError()).entity(mensaje)
				.build();
	}

	@PUT
	@Path("actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	public final Response editarMultimedia(PeliculasBean pelis,
			@QueryParam(value = "id") int id) {

		MensajesBean mensaje = new MensajesBean();

		try {
			peliculasDao.actualizarMultimedia(pelis, id);
			mensaje.setMensaje("ok");
			mensaje.setCodigoError(200);
		} catch (ClassNotFoundException e) {
			mensaje.setMensaje(e.getMessage());
			mensaje.setCodigoError(500);
		} catch (SQLException e) {
			mensaje.setMensaje(e.getMessage());
			mensaje.setCodigoError(500);
		}

		return Response.status(mensaje.getCodigoError()).entity(mensaje)
				.build();

	}

	@GET
	@Path("/informe")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getInforme() {
		JasperReport jasperReport;
		JasperPrint jasperPrint;
		JRExporter exporter = new JRPdfExporter();
		FileOutputStream exportado = null;
		MensajesBean mensaje = null;

		try {
			VariablesComunes.conexion.setAutoCommit(false);

			// System.out.println(System.getProperty("catalina.base"));

			jasperReport = JasperCompileManager
					.compileReport("D:\\Programacion\\Programacion Java\\PeliculasySeries\\Informes\\InformeGeneral.jrxml");

			jasperPrint = JasperFillManager.fillReport(jasperReport, null,
					VariablesComunes.conexion);

			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exportado = new FileOutputStream(Constantes.RUTASERVIDORPDF
					+ "pdf//informegeneral.pdf");

			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, exportado);

			exporter.exportReport();

			mensaje = new MensajesBean();
			mensaje.setCodigoError(200);
			mensaje.setMensaje("../pdf/informegeneral.pdf");
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JRException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				VariablesComunes.conexion.setAutoCommit(true);
				exporter = null;
				exportado.close();
			} catch (SQLException | IOException e) {
				e.printStackTrace();
			}
		}

		return Response.status(mensaje.getCodigoError()).entity(mensaje)
				.build();
	}

	@GET
	@Path("/buscar")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public Response buscar(@QueryParam(value = "q") String parametro) {

		ArrayList<PeliculasBean> pelis = null;
		if (null == parametro) {
			parametro = "";
		}
		try {
			pelis = peliculasDao.busqueda(parametro);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return Response.status(200).entity(pelis).build();
	}

}
