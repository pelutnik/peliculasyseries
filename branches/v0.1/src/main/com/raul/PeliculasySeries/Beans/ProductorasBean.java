package com.raul.PeliculasySeries.Beans;

public class ProductorasBean {
	private String clave;
	private String nombre;
	private int id;
	private String descripcion;

	public ProductorasBean(String clave, String nombre, int id) {
		super();
		this.clave = clave;
		this.nombre = nombre;
		this.id = id;
	}

	public ProductorasBean(String clave, String nombre, int id,
			String descripcion) {
		super();
		this.clave = clave;
		this.nombre = nombre;
		this.id = id;
		this.descripcion = descripcion;
	}

	public ProductorasBean() {
		super();
		/* constructor vacio */
		this.clave = "";
		this.nombre = "";
		this.id = 0;
		this.descripcion = "";

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return "ProductorasBean [id=" + id + ", clave=" + clave + ", nombre="
				+ nombre + ", descripcion=" + descripcion + "]";
	}

}
