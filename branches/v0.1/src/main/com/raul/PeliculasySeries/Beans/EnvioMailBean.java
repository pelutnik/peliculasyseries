package com.raul.PeliculasySeries.Beans;

import com.raul.PeliculasySeries.Variables.Constantes;

public class EnvioMailBean {

	private String archivoEnviar;
	private String email;
	private String mensaje;
	private String fileName;

	/**
	 * 
	 */
	public EnvioMailBean() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param archivoEnviar
	 * @param email
	 * @param msg
	 */
	public EnvioMailBean(String archivoEnviar, String email, String mensaje) {
		super();
		this.setArchivoEnviar(archivoEnviar);
		this.setEmail(email);
		this.setMensaje(mensaje);
	}

	public String getArchivoEnviar() {
		return Constantes.RUTASERVIDORPDF + archivoEnviar;
	}

	public void setArchivoEnviar(String archivoEnviar) {
		this.archivoEnviar = archivoEnviar.replace("../", "")
				.replace("/", "\\");

		this.setFileName(archivoEnviar.substring(archivoEnviar.lastIndexOf("/") + 1));
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	@Override
	public String toString() {
		return "EnvioMailBean [archivoEnviar=" + this.getArchivoEnviar()
				+ ", email=" + this.getEmail() + ", msg=" + this.getMensaje()
				+ "]";
	}

}
