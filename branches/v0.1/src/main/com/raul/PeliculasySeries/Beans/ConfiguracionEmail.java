package com.raul.PeliculasySeries.Beans;

public class ConfiguracionEmail {
	private String smtpHost;
	private int smtpPort;
	private String mailUser;
	private String mailPass;
	private String mailSender;
	private String mailName;

	/**
	 * 
	 */
	public ConfiguracionEmail() {
		smtpHost = "";
		smtpPort = 0;
		mailUser = "";
		mailPass = "";
		mailSender = "";
	}

	/**
	 * @param smtpHost
	 * @param smtpPort
	 * @param mailUser
	 * @param mailPass
	 * @param mailSender
	 * @param mailName
	 */
	public ConfiguracionEmail(String smtpHost, int smtpPort, String mailUser,
			String mailPass, String mailSender, String mailName) {
		super();
		this.smtpHost = smtpHost;
		this.smtpPort = smtpPort;
		this.mailUser = mailUser;
		this.mailPass = mailPass;
		this.mailSender = mailSender;
		this.mailName = mailName;
	}

	/**
	 * @return the smtpHost
	 */
	public String getSmtpHost() {
		return smtpHost;
	}

	/**
	 * @param smtpHost
	 *            the smtpHost to set
	 */
	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}

	/**
	 * @return the smtpPort
	 */
	public int getSmtpPort() {
		return smtpPort;
	}

	/**
	 * @param smtpPort
	 *            the smtpPort to set
	 */
	public void setSmtpPort(int smtpPort) {
		this.smtpPort = smtpPort;
	}

	/**
	 * @return the mailUser
	 */
	public String getMailUser() {
		return mailUser;
	}

	/**
	 * @param mailUser
	 *            the mailUser to set
	 */
	public void setMailUser(String mailUser) {
		this.mailUser = mailUser;
	}

	/**
	 * @return the mailPass
	 */
	public String getMailPass() {
		return mailPass;
	}

	/**
	 * @param mailPass
	 *            the mailPass to set
	 */
	public void setMailPass(String mailPass) {
		this.mailPass = mailPass;
	}

	/**
	 * @return the mailSender
	 */
	public String getMailSender() {
		return mailSender;
	}

	/**
	 * @param mailSender
	 *            the mailSender to set
	 */
	public void setMailSender(String mailSender) {
		this.mailSender = mailSender;
	}

	/**
	 * @return the mailName
	 */
	public String getMailName() {
		return mailName;
	}

	/**
	 * @param mailName
	 *            the mailName to set
	 */
	public void setMailName(String mailName) {
		this.mailName = mailName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ConfiguracionEmail [smtpHost=" + smtpHost + ", smtpPort="
				+ smtpPort + ", mailUser=" + mailUser + ", mailPass="
				+ mailPass + ", mailSender=" + mailSender + ", mailName="
				+ mailName + "]";
	}

}
