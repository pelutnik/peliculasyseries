package com.raul.PeliculasySeries.Beans;

public class GenerosBean {
	private String clave;
	private String nombre;
	private int id;
	private String descripcion;

	public GenerosBean(int id, String clave, String nombre) {
		super();
		this.clave = clave;
		this.nombre = nombre;
		this.id = id;
	}

	public GenerosBean() {
		super();
		/* constructor vacio */
		this.clave = "";
		this.nombre = "";
		this.id = 0;
		this.descripcion = "";
	}

	public GenerosBean(String clave, String nombre, int id, String descripcion) {
		super();
		this.clave = clave;
		this.nombre = nombre;
		this.id = id;
		this.descripcion = descripcion;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GenerosBean [clave=" + clave + ", nombre=" + nombre + ", id="
				+ id + ", descripcion=" + descripcion + "]";
	}

}
