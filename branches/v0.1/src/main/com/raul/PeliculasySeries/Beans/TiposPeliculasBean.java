package com.raul.PeliculasySeries.Beans;

public class TiposPeliculasBean {
	private int id;
	private String clave;
	private String nombre;
	private String descripcion;

	/**
	 * 
	 * @param clave
	 * @param nombre
	 * @param ident
	 */
	public TiposPeliculasBean(final String clave, final String nombre,
			final int ident) {
		super();
		this.clave = clave;
		this.nombre = nombre;
		this.id = ident;
	}

	/**
	 * 
	 * @param ident
	 * @param clave
	 * @param nombre
	 * @param descripcion
	 */
	public TiposPeliculasBean(final int ident, final String clave,
			final String nombre, String descripcion) {
		super();
		this.id = ident;
		this.clave = clave;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}

	/**
 * 
 */
	public TiposPeliculasBean() {
		/* constructor vacio */
		this.clave = "";
		this.nombre = "";
		this.id = 0;
	}

	/**
	 * 
	 */
	public int getId() {
		return id;
	}

	/**
	 * 
	 * @param ident
	 */
	public void setId(final int ident) {
		this.id = ident;
	}

	/**
	 * 
	 * @return
	 */
	public String getClave() {
		return clave;
	}

	/**
	 * 
	 * @param clave
	 */
	public void setClave(final String clave) {
		this.clave = clave;
	}

	public String getNombre() {
		return nombre;
	}

	/**
	 * 
	 * @param nombre
	 */
	public void setNombre(final String nombre) {
		this.nombre = nombre;
	}

	/**
	 * 
	 * @return
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * 
	 * @param descripcion
	 */
	public void setDescripcion(final String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	/**
	 * 
	 */
	public String toString() {
		return "TiposPeliculasBean [id=" + id + ", clave=" + clave
				+ ", nombre=" + nombre + ", descripcion=" + descripcion + "]";
	}

}
