package com.raul.PeliculasySeries.Beans;

public class EstadisticasProductora {
	private String Nombre;
	private int Contador;

	public EstadisticasProductora() {
		super();
		this.Nombre = "";
		this.Contador = 0;
	}

	public EstadisticasProductora(String productora, int numPelis) {
		super();
		this.setProductora(productora);
		this.setNumPelis(numPelis);
	}

	public String getProductora() {
		return Nombre;
	}

	public void setProductora(String productora) {
		this.Nombre = productora;
	}

	public int getNumPelis() {
		return Contador;
	}

	public void setNumPelis(int numPelis) {
		this.Contador = numPelis;
	}

	@Override
	public String toString() {
		return "EstadisticasProductora [$F{Nombre=" + this.Nombre
				+ "}, $F{contador=" + this.Contador + "}]";
	}

}
