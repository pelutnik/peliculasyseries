package com.raul.PeliculasySeries.Beans;

public class BusquedasBean {
	private String value;
	private String data;

	public BusquedasBean() {

	}

	/**
	 * @param value
	 * @param data
	 */
	public BusquedasBean(String value, String data) {
		super();
		this.value = value;
		this.data = data;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(String data) {
		this.data = data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BusquedasBean [value=" + this.getValue() + ", data="
				+ this.getData() + "]";
	}
}
