/**
 * 
 */
package com.raul.PeliculasySeries.Beans;

/**
 * @author Raul
 *
 */
public class EstadosBean {

	private int id;
	private String clave;
	private String nombre;
	private String descripcion;

	/**
	 * 
	 * @param clave
	 * @param nombre
	 * @param ident
	 */
	public EstadosBean(final String clave, final String nombre, final int id) {
		super();
		this.clave = clave;
		this.nombre = nombre;
		this.id = id;
	}

	/**
	 * 
	 * @param ident
	 * @param clave
	 * @param nombre
	 * @param descripcion
	 */
	public EstadosBean(final int id, final String clave, final String nombre,
			String descripcion) {
		super();
		this.id = id;
		this.clave = clave;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}

	/**
 * 
 */
	public EstadosBean() {
		/* constructor vacio */
		this.clave = "";
		this.nombre = "";
		this.id = 0;
	}

	/**
	 * 
	 */
	public int getId() {
		return id;
	}

	/**
	 * 
	 * @param ident
	 */
	public void setId(final int id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	public String getClave() {
		return clave;
	}

	/**
	 * 
	 * @param clave
	 */
	public void setClave(final String clave) {
		this.clave = clave;
	}

	public String getNombre() {
		return nombre;
	}

	/**
	 * 
	 * @param nombre
	 */
	public void setNombre(final String nombre) {
		this.nombre = nombre;
	}

	/**
	 * 
	 * @return
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * 
	 * @param descripcion
	 */
	public void setDescripcion(final String descripcion) {
		this.descripcion = descripcion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EstadosBean [id=" + id + ", clave=" + clave + ", nombre="
				+ nombre + ", descripcion=" + descripcion + "]";
	}

}
