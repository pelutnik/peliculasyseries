package com.raul.PeliculasySeries.Beans;

public class MensajesBean {

	private String mensaje;
	private int codigoError;

	public int getCodigoError() {
		return codigoError;
	}

	public void setCodigoError(int codigoError) {
		this.codigoError = codigoError;
	}

	public MensajesBean() {

	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	@Override
	public String toString() {
		return "MensajesBean [mensaje=" + mensaje + ", codigoError="
				+ codigoError + "]";
	}

}