package com.raul.PeliculasySeries.Beans;

public class PeliculasBean {
	private int id;
	private String clave;
	private String titulo;
	private String formato;
	private String tipo;
	private String productora;
	private int temporada;
	private String sipnosis;
	private String estado;
	private String genero;

	/**
	 * 
	 */
	public PeliculasBean() {

	}

	/**
	 * @param id
	 * @param clave
	 * @param titulo
	 * @param formato
	 * @param tipo
	 * @param productora
	 * @param temporada
	 * @param sipnosis
	 * @param estado
	 */
	public PeliculasBean(int id, String titulo, String productora,
			int temporada, String formato, String clave, String tipo,
			String sipnosis, String estado, String genero) {
		super();
		this.id = id;
		this.clave = clave;
		this.titulo = titulo;
		this.formato = formato;
		this.tipo = tipo;
		this.productora = productora;
		this.temporada = temporada;
		this.sipnosis = sipnosis;
		this.estado = estado;
		this.genero = genero;
	}

	/**
	 * @param id
	 * @param clave
	 * @param titulo
	 * @param formato
	 * @param tipo
	 * @param productora
	 * @param temporada
	 * @param sipnosis
	 * @param estado
	 */
	public PeliculasBean(String estado, int id, String titulo,
			String productora, int temporada, String formato, String tipo,
			String clave, String genero) {
		super();
		this.id = id;
		this.clave = clave;
		this.titulo = titulo;
		this.formato = formato;
		this.tipo = tipo;
		this.productora = productora;
		this.temporada = temporada;
		this.estado = estado;
		this.genero = genero;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the clave
	 */
	public String getClave() {
		return clave;
	}

	/**
	 * @param clave
	 *            the clave to set
	 */
	public void setClave(String clave) {
		this.clave = clave;
	}

	/**
	 * @return the titulo
	 */
	public String getTitulo() {
		return titulo;
	}

	/**
	 * @param titulo
	 *            the titulo to set
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	/**
	 * @return the formato
	 */
	public String getFormato() {
		return formato;
	}

	/**
	 * @param formato
	 *            the formato to set
	 */
	public void setFormato(String formato) {
		this.formato = formato;
	}

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo
	 *            the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the productora
	 */
	public String getProductora() {
		return productora;
	}

	/**
	 * @param productora
	 *            the productora to set
	 */
	public void setProductora(String productora) {
		this.productora = productora;
	}

	/**
	 * @return the temporada
	 */
	public int getTemporada() {
		return temporada;
	}

	/**
	 * @param temporada
	 *            the temporada to set
	 */
	public void setTemporada(int temporada) {
		this.temporada = temporada;
	}

	/**
	 * @return the sipnosis
	 */
	public String getSipnosis() {
		return sipnosis;
	}

	/**
	 * @param sipnosis
	 *            the sipnosis to set
	 */
	public void setSipnosis(String sipnosis) {
		this.sipnosis = sipnosis;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado
	 *            the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the genero
	 */
	public String getGenero() {
		return genero;
	}

	/**
	 * @param genero
	 *            the genero to set
	 */
	public void setGenero(String genero) {
		this.genero = genero;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PeliculasBean [id=" + id + ", clave=" + clave + ", titulo="
				+ titulo + ", formato=" + formato + ", tipo=" + tipo
				+ ", productora=" + productora + ", temporada=" + temporada
				+ ", sipnosis=" + sipnosis + ", estado=" + estado + ", genero="
				+ genero + "]";
	}

}
