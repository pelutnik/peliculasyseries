package com.raul.PeliculasySeries.core.FrontControler;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import com.raul.PeliculasySeries.Restful.EnvioMail;
import com.raul.PeliculasySeries.Restful.Estados;
import com.raul.PeliculasySeries.Restful.Formatos;
import com.raul.PeliculasySeries.Restful.Generos;
import com.raul.PeliculasySeries.Restful.Informes;
import com.raul.PeliculasySeries.Restful.PeliculasYSeries;
import com.raul.PeliculasySeries.Restful.Productoras;
import com.raul.PeliculasySeries.Restful.TiposPeliculas;
import com.raul.PeliculasySeries.Variables.Constantes;
import com.raul.PeliculasySeries.Variables.VariablesComunes;

/***/
@ApplicationPath("server")
public class FrontControler extends Application {
	@Override
	/**
	 * Aqui van todas las referencias a las clases de los controllers para que se registren en el Servicio Restful
	 */
	public final Set<Class<?>> getClasses() {
		final Set<Class<?>> classes = new HashSet<Class<?>>();
		// Establecer la conexion con la base de datos para mantenerla en todo
		// el programa
		Properties prop = null;
		FileInputStream fis = null;
		prop = new Properties();
		try {
			fis = new FileInputStream(Constantes.RUTASERVIDORCONF
					+ "\\db.properties");
			prop.load(fis);

			VariablesComunes.database = prop.getProperty("database");
			VariablesComunes.pass = prop.getProperty("pass");
			VariablesComunes.user = prop.getProperty("user");
			VariablesComunes.port = prop.getProperty("port");
			VariablesComunes.server = prop.getProperty("server");

			VariablesComunes.urlConexion = "jdbc:sqlserver://"
					+ VariablesComunes.server + ":" + VariablesComunes.port
					+ ";databaseName=" + VariablesComunes.database;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Inicio registro de las clases controladoras

		/*
		 * classes.add(com.wordnik.swagger.jaxrs.JaxrsApiReader.class);
		 * classes.add
		 * (com.wordnik.swagger.jaxrs.listing.ApiListingResource.class);
		 * classes.
		 * add(com.wordnik.swagger.jaxrs.listing.ApiDeclarationProvider.class);
		 * classes
		 * .add(com.wordnik.swagger.jaxrs.listing.ApiListingResourceJSON.class);
		 * classes
		 * .add(com.wordnik.swagger.jaxrs.listing.ResourceListingProvider.
		 * class);
		 */

		addRestResourceClasses(classes);
		// Fin del registro de las clases controladoras.
		return classes;
	}

	/**
	 * Do not modify addRestResourceClasses() method. It is automatically
	 * populated with all resources defined in the project. If required, comment
	 * out calling this method in getClasses().
	 */
	private void addRestResourceClasses(Set<Class<?>> classes) {
		/**
		 * Registro de la clase PeliculasYSeries
		 */
		classes.add(PeliculasYSeries.class);
		classes.add(Formatos.class);
		classes.add(TiposPeliculas.class);
		classes.add(Productoras.class);
		classes.add(Informes.class);
		classes.add(EnvioMail.class);
		classes.add(Estados.class);
		classes.add(Generos.class);
		// classes.add(UploadFiles.class);
	}
}
