package com.raul.PeliculasySeries.Configs;

import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.raul.PeliculasySeries.Variables.Constantes;

/**
 * 
 * @author Raul
 *
 */
public class LoggerConfig {
	/**
	 * 
	 */
	static final String RUTA_PROPIEDADES_LOG = Constantes.RUTASERVIDORCONF
			+ "\\log4j.properties";

	/**
	 * 
	 * @param nombreClase
	 *            : Nombre de la clase que configura el log
	 * @return Devuelve el objeto de tipo logger
	 */
	public static Logger configurarLog(final String nombreClase) {
		Logger log;

		PropertyConfigurator.configure(RUTA_PROPIEDADES_LOG);

		log = LoggerFactory.getLogger(nombreClase);

		return log;

	}
}
