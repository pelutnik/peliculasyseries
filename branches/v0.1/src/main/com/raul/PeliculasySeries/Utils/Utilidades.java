package com.raul.PeliculasySeries.Utils;

import java.sql.DriverManager;
import java.sql.SQLException;

import com.raul.PeliculasySeries.Variables.Constantes;
import com.raul.PeliculasySeries.Variables.VariablesComunes;

public class Utilidades {
	public static void conectar() throws ClassNotFoundException, SQLException {

		if (VariablesComunes.conexion == null) {

			Class.forName(Constantes.DRIVER);

			VariablesComunes.conexion = DriverManager.getConnection(
					VariablesComunes.urlConexion, VariablesComunes.user,
					VariablesComunes.pass);
		}
	}
}
