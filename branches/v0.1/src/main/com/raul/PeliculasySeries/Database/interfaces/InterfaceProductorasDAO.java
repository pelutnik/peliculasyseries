package com.raul.PeliculasySeries.Database.interfaces;

import java.sql.SQLException;
import java.util.ArrayList;

import com.raul.PeliculasySeries.Beans.ProductorasBean;

public interface InterfaceProductorasDAO {
	public ArrayList<ProductorasBean> obtenerDatos()
			throws ClassNotFoundException, SQLException;

	public ProductorasBean consultaUnaProductora(int id)
			throws ClassNotFoundException, SQLException;

	public boolean insertarProductora(ProductorasBean productora)
			throws ClassNotFoundException, SQLException;

	public boolean actualizarRegistro(ProductorasBean productora, int id)
			throws ClassNotFoundException, SQLException;

	public boolean borrarRegistro(int id) throws ClassNotFoundException,
			SQLException;
}
