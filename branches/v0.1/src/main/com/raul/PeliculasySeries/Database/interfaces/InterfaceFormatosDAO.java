package com.raul.PeliculasySeries.Database.interfaces;

import java.sql.SQLException;
import java.util.ArrayList;

import com.raul.PeliculasySeries.Beans.FormatosBean;

public interface InterfaceFormatosDAO {
	public ArrayList<FormatosBean> obtenerDatos()
			throws ClassNotFoundException, SQLException;

	public FormatosBean consultarUnFormato(int id)
			throws ClassNotFoundException, SQLException;

	public boolean insertarFormato(FormatosBean formato)
			throws ClassNotFoundException, SQLException;

	public boolean actualizarRegistro(FormatosBean formato, int id)
			throws ClassNotFoundException, SQLException;

	public boolean borrarRegistro(int id) throws ClassNotFoundException,
			SQLException;
}
