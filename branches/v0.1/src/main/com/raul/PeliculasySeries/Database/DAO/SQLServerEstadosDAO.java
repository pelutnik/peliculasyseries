package com.raul.PeliculasySeries.Database.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.raul.PeliculasySeries.Beans.EstadosBean;
import com.raul.PeliculasySeries.Database.Consultas.Consultas;
import com.raul.PeliculasySeries.Database.interfaces.InterfaceEstadosDAO;
import com.raul.PeliculasySeries.Variables.VariablesComunes;

public class SQLServerEstadosDAO implements InterfaceEstadosDAO {

	/**
	 * 
	 */
	public SQLServerEstadosDAO() {

	}

	@Override
	public ArrayList<EstadosBean> obtenerDAtos() throws ClassNotFoundException,
			SQLException {
		ArrayList<EstadosBean> res = new ArrayList<EstadosBean>();

		Statement statement = VariablesComunes.conexion.createStatement();
		ResultSet rs = statement.executeQuery(Consultas.CONSULTATODOSESTADOS);

		while (rs.next()) {
			EstadosBean estado = new EstadosBean(rs.getString("clave"),
					rs.getString("nombre"), rs.getInt("id"));
			res.add(estado);
		}

		return res;
	}

	@Override
	public EstadosBean consultaUnEstado(int id) throws ClassNotFoundException,
			SQLException {

		EstadosBean res = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		pst = VariablesComunes.conexion
				.prepareStatement(Consultas.CONSULTAUNESTADO);
		pst.setInt(1, id);

		rs = pst.executeQuery();

		while (rs.next()) {
			res = new EstadosBean(rs.getInt("id"), rs.getString("clave"),
					rs.getString("nombre"), rs.getString("descripcion"));
		}

		return res;
	}

	@Override
	public boolean actualizarRegistro(EstadosBean estado, int id)
			throws ClassNotFoundException, SQLException {
		boolean res;
		PreparedStatement pst = null;

		pst = VariablesComunes.conexion
				.prepareStatement(Consultas.CONSULTAACTUALIZACIONESTADO);
		pst.setString(1, estado.getClave());
		pst.setString(2, estado.getNombre());
		pst.setString(3, estado.getDescripcion());
		pst.setInt(4, id);

		res = pst.execute();

		return res;
	}

	@Override
	public boolean insertarEstado(EstadosBean estado)
			throws ClassNotFoundException, SQLException {

		boolean res = false;
		PreparedStatement pst = null;

		pst = VariablesComunes.conexion
				.prepareStatement(Consultas.CONSULTAINSERCIONESTADO);
		pst.setString(1, estado.getClave());
		pst.setString(2, estado.getNombre());
		pst.setString(3, estado.getDescripcion());

		res = pst.execute();

		res = true;

		pst.close();

		return res;
	}

	@Override
	public boolean borrarRegistro(int id) throws ClassNotFoundException,
			SQLException {
		PreparedStatement pst = null;

		pst = VariablesComunes.conexion
				.prepareStatement(Consultas.CONSULTABORRADOESTADOS);
		pst.setInt(1, id);

		return pst.execute();
	}

}
