package com.raul.PeliculasySeries.Database.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.raul.PeliculasySeries.Beans.PeliculasBean;
import com.raul.PeliculasySeries.Database.Consultas.Consultas;
import com.raul.PeliculasySeries.Database.interfaces.InterfacePeliculasDAO;
import com.raul.PeliculasySeries.Variables.VariablesComunes;

public class SQLServerPeliculasDAO implements InterfacePeliculasDAO {

	/**
	 * Obtenemos todos los datos de las peliculas para crear una tabla en la
	 * pagina principal
	 * 
	 * @return
	 */
	@Override
	public ArrayList<PeliculasBean> obtenerDatos()
			throws ClassNotFoundException, SQLException {
		ArrayList<PeliculasBean> res = new ArrayList<PeliculasBean>();

		Statement statement = VariablesComunes.conexion.createStatement();
		ResultSet rs = statement.executeQuery(Consultas.CONSULTATODASPELICULAS);

		while (rs.next()) {
			/*
			 * PeliculasBean pelis = new PeliculasBean(rs.getString("Estado"),
			 * rs.getInt("Id"), rs.getString("Nombre"),
			 * rs.getString("Productora"), rs.getInt("Temporada"),
			 * rs.getString("Formato"), rs.getString("Tipo"),
			 * rs.getString("Clave"), rs.getString("Genero")); //
			 * System.out.println(pelis.toString());
			 */

			res.add(instanciarPeliculas(rs));
		}

		return res;
	}

	@Override
	public PeliculasBean cargarPelicula(int id, boolean edicion)
			throws ClassNotFoundException, SQLException {
		PeliculasBean res = null;
		PreparedStatement pst = null;

		if (edicion) {
			pst = VariablesComunes.conexion
					.prepareStatement(Consultas.CONSULTAUNAPELICULAEDICION);
		} else {
			pst = VariablesComunes.conexion
					.prepareStatement(Consultas.CONSULTAUNAPELICULADETALLE);
		}
		pst.setInt(1, id);
		ResultSet rs = pst.executeQuery();

		// System.out.println(rs.toString());
		// rs.first();
		while (rs.next()) {
			res = instanciarPeliculas(rs);
			/*
			 * res = new PeliculasBean(rs.getInt("Id"), rs.getString("Nombre"),
			 * rs.getString("Productora"), rs.getInt("Temporada"),
			 * rs.getString("Formato"), rs.getString("Clave"),
			 * rs.getString("Tipo"), rs.getString("Sipnosis"),
			 * rs.getString("Estado"), rs.getString("Generos"));
			 */
		}

		return res;
	}

	@Override
	public boolean insertarMultimedia(PeliculasBean pelis)
			throws ClassNotFoundException, SQLException {

		boolean res = false;
		PreparedStatement pst = null;

		pst = VariablesComunes.conexion
				.prepareStatement(Consultas.CONSULTAINSERCIONPELICULAS);
		pst.setString(1, pelis.getClave());
		pst.setString(2, pelis.getTitulo());
		pst.setString(3, pelis.getFormato());
		pst.setString(4, pelis.getTipo());
		pst.setString(5, pelis.getProductora());
		pst.setInt(6, pelis.getTemporada());
		pst.setString(7, pelis.getSipnosis());
		pst.setString(8, pelis.getEstado());
		pst.setString(9, pelis.getGenero());
		res = pst.execute();

		res = true;

		return res;

	}

	@Override
	public boolean actualizarMultimedia(PeliculasBean pelis, int id)
			throws ClassNotFoundException, SQLException {
		boolean res = true;

		PreparedStatement pst = null;

		pst = VariablesComunes.conexion
				.prepareStatement(Consultas.CONSULTAACTUALIZACIONPELICULA);

		pst.setString(1, pelis.getClave());
		pst.setString(2, pelis.getTitulo());
		pst.setString(3, pelis.getFormato());
		pst.setString(4, pelis.getTipo());
		pst.setString(5, pelis.getProductora());
		pst.setInt(6, pelis.getTemporada());
		pst.setString(7, pelis.getSipnosis());
		pst.setString(8, pelis.getEstado());
		pst.setString(9, pelis.getGenero());
		pst.setInt(10, id);
		res = pst.execute();

		res = true;

		return res;
	}

	@Override
	public boolean borrarRegistro(int id) throws ClassNotFoundException,
			SQLException {
		PreparedStatement pst = null;

		pst = VariablesComunes.conexion
				.prepareStatement(Consultas.CONSULTABORRADOPELICULAS);
		pst.setInt(1, id);

		return pst.execute();
	}

	@Override
	public ArrayList<PeliculasBean> busqueda(String parametro)
			throws ClassNotFoundException, SQLException {
		ArrayList<PeliculasBean> res = new ArrayList<PeliculasBean>();
		PreparedStatement pst = null;

		pst = VariablesComunes.conexion
				.prepareStatement("SELECT id as Id,clave as Clave, nombre as Nombre , tipo as Tipo, productoras as Productora, formato as Formato, estados as Estado, generos as Genero, temporada as Temporada, sipnosis as Sipnosis FROM peliculas WHERE nombre LIKE '%"
						+ parametro + "%'");

		// pst.setString(1, parametro);

		ResultSet rs = pst.executeQuery();

		while (rs.next()) {

			/*
			 * PeliculasBean temp = new BusquedasBean(rs.getString("Clave"),
			 * rs.getString("Nombre"));
			 */
			// System.out.println(temp.toString());
			res.add(instanciarPeliculas(rs));
		}

		return res;
	}

	private PeliculasBean instanciarPeliculas(ResultSet rs)
			throws ClassNotFoundException, SQLException {

		PeliculasBean pelis = new PeliculasBean();

		pelis.setId(rs.getInt("Id"));
		pelis.setTitulo(rs.getString("Nombre"));
		pelis.setProductora(rs.getString("Productora"));
		pelis.setTemporada(rs.getInt("Temporada"));
		pelis.setFormato(rs.getString("Formato"));
		pelis.setTipo(rs.getString("Tipo"));
		pelis.setClave(rs.getString("Clave"));
		pelis.setGenero(rs.getString("Genero"));
		pelis.setSipnosis(rs.getString("Sipnosis"));
		pelis.setEstado(rs.getString("Estado"));

		return pelis;
	}
}
