package com.raul.PeliculasySeries.Database.interfaces;

import java.sql.SQLException;
import java.util.ArrayList;

import com.raul.PeliculasySeries.Beans.PeliculasBean;

public interface InterfacePeliculasDAO {
	public ArrayList<PeliculasBean> obtenerDatos()
			throws ClassNotFoundException, SQLException;

	public PeliculasBean cargarPelicula(int id, boolean edicion)
			throws ClassNotFoundException, SQLException;

	public boolean insertarMultimedia(PeliculasBean pelis)
			throws ClassNotFoundException, SQLException;

	public boolean actualizarMultimedia(PeliculasBean pelis, int id)
			throws ClassNotFoundException, SQLException;

	public boolean borrarRegistro(int id) throws ClassNotFoundException,
			SQLException;

	public ArrayList<PeliculasBean> busqueda(String parametro)
			throws ClassNotFoundException, SQLException;
}
