package com.raul.PeliculasySeries.Database.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.raul.PeliculasySeries.Beans.FormatosBean;
import com.raul.PeliculasySeries.Database.Consultas.Consultas;
import com.raul.PeliculasySeries.Database.interfaces.InterfaceFormatosDAO;
import com.raul.PeliculasySeries.Variables.VariablesComunes;

public class SQLServerFormatosDAO implements InterfaceFormatosDAO {

	/**
	 * Obtenemos todos los datos de las peliculas para crear una tabla en la
	 * pagina principal
	 * 
	 * @return
	 */
	@Override
	public ArrayList<FormatosBean> obtenerDatos()
			throws ClassNotFoundException, SQLException {
		ArrayList<FormatosBean> res = new ArrayList<FormatosBean>();

		Statement statement = VariablesComunes.conexion.createStatement();
		ResultSet rs = statement.executeQuery(Consultas.CONSULTATODOSFORMATOS);

		// System.out.println(rs.toString());
		// rs.first();
		while (rs.next()) {
			FormatosBean formato = new FormatosBean(rs.getInt("id"),
					rs.getString("clave"), rs.getString("nombre"));
			// System.out.println(formato.toString());

			res.add(formato);
		}

		return res;
	}

	/**
	 * Inserta un nuevo registro en la BD
	 * 
	 * @param formato
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	@Override
	public boolean insertarFormato(FormatosBean formato)
			throws ClassNotFoundException, SQLException {

		boolean res = false;
		PreparedStatement pst = null;

		pst = VariablesComunes.conexion
				.prepareStatement(Consultas.CONSULTAINSERCIONFORMATOS);
		pst.setString(1, formato.getClave());
		pst.setString(2, formato.getNombre());
		pst.setString(3, formato.getDescripcion());

		res = pst.execute();

		res = true;

		return res;

	}

	@Override
	public FormatosBean consultarUnFormato(int id)
			throws ClassNotFoundException, SQLException {
		FormatosBean res = null;

		PreparedStatement pst = null;
		ResultSet rs = null;

		pst = VariablesComunes.conexion
				.prepareStatement(Consultas.CONSULTAUNFORMATO);
		pst.setInt(1, id);

		rs = pst.executeQuery();

		while (rs.next()) {
			res = new FormatosBean(rs.getString("clave"),
					rs.getString("nombre"), rs.getInt("id"),
					rs.getString("descripcion"));
		}

		return res;
	}

	@Override
	public boolean actualizarRegistro(FormatosBean formato, int id)
			throws ClassNotFoundException, SQLException {
		boolean res;
		PreparedStatement pst = null;

		pst = VariablesComunes.conexion
				.prepareStatement(Consultas.CONSULTAACTUALIZACIONFORMATO);
		pst.setString(1, formato.getClave());
		pst.setString(2, formato.getNombre());
		pst.setString(3, formato.getDescripcion());
		pst.setInt(4, id);

		res = pst.execute();

		return res;
	}

	@Override
	public boolean borrarRegistro(int id) throws ClassNotFoundException,
			SQLException {
		PreparedStatement pst = null;

		pst = VariablesComunes.conexion
				.prepareStatement(Consultas.CONSULTABORRADOFORMATOS);
		pst.setInt(1, id);

		return pst.execute();
	}
}
