package com.raul.PeliculasySeries.Database.interfaces;

import java.sql.SQLException;
import java.util.ArrayList;

import com.raul.PeliculasySeries.Beans.TiposPeliculasBean;

public interface InterfaceCategoriasDAO {
	public ArrayList<TiposPeliculasBean> obtenerDatos()
			throws ClassNotFoundException, SQLException;

	public TiposPeliculasBean consultaUnTipoPelicula(int id)
			throws ClassNotFoundException, SQLException;

	public boolean insertarTipo(TiposPeliculasBean tipo)
			throws ClassNotFoundException, SQLException;

	public boolean actualizarRegistro(TiposPeliculasBean tipo, int id)
			throws ClassNotFoundException, SQLException;

	public boolean borrarRegistro(int id) throws ClassNotFoundException,
			SQLException;
}
