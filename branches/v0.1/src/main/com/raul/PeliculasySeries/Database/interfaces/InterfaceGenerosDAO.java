package com.raul.PeliculasySeries.Database.interfaces;

import java.sql.SQLException;
import java.util.ArrayList;

import com.raul.PeliculasySeries.Beans.GenerosBean;

public interface InterfaceGenerosDAO {

	public ArrayList<GenerosBean> obtenerDatos() throws ClassNotFoundException,
			SQLException;

	public GenerosBean consultaUnGenero(int id) throws ClassNotFoundException,
			SQLException;

	public boolean insertarGenero(GenerosBean genero)
			throws ClassNotFoundException, SQLException;

	public boolean actualizarRegistro(GenerosBean genero, int id)
			throws ClassNotFoundException, SQLException;

	public boolean borrarRegistro(int id) throws ClassNotFoundException,
			SQLException;

}
