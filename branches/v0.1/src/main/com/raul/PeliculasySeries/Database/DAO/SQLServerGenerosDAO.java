package com.raul.PeliculasySeries.Database.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.raul.PeliculasySeries.Beans.GenerosBean;
import com.raul.PeliculasySeries.Database.Consultas.Consultas;
import com.raul.PeliculasySeries.Database.interfaces.InterfaceGenerosDAO;
import com.raul.PeliculasySeries.Variables.VariablesComunes;

public class SQLServerGenerosDAO implements InterfaceGenerosDAO {

	/**
	 * Obtenemos todos los datos de las peliculas para crear una tabla en la
	 * pagina principal
	 * 
	 * @return
	 * @throws SQLException
	 *             , ClassNotFoundException
	 */
	@Override
	public ArrayList<GenerosBean> obtenerDatos() throws SQLException,
			ClassNotFoundException {
		ArrayList<GenerosBean> res = new ArrayList<GenerosBean>();

		Statement statement = VariablesComunes.conexion.createStatement();
		ResultSet rs = statement.executeQuery(Consultas.CONSULTATODOSGENEROS);

		// System.out.println(rs.toString());
		// rs.first();
		while (rs.next()) {
			GenerosBean genero = new GenerosBean(rs.getString("clave"),
					rs.getString("nombre"), rs.getInt("id"),
					rs.getString("descripcion"));
			// System.out.println(productoras.toString());

			res.add(genero);
		}

		return res;
	}

	@Override
	public GenerosBean consultaUnGenero(int id) throws ClassNotFoundException,
			SQLException {
		GenerosBean res = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		pst = VariablesComunes.conexion
				.prepareStatement(Consultas.CONSULTAUNGENERO);
		pst.setInt(1, id);

		rs = pst.executeQuery();

		while (rs.next()) {
			res = new GenerosBean(rs.getString("clave"),
					rs.getString("nombre"), rs.getInt("id"),
					rs.getString("descripcion"));
		}

		return res;
	}

	/**
	 * Inserta un registro en la BD
	 * 
	 * @param productora
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	@Override
	public boolean insertarGenero(GenerosBean genero)
			throws ClassNotFoundException, SQLException {

		boolean res = false;
		PreparedStatement pst = null;

		pst = VariablesComunes.conexion
				.prepareStatement(Consultas.CONSULTAINSERCIONGENERO);
		pst.setString(1, genero.getClave());
		pst.setString(2, genero.getNombre());
		pst.setString(3, genero.getDescripcion());

		res = pst.execute();

		res = true;

		pst.close();

		return res;

	}

	@Override
	public boolean actualizarRegistro(GenerosBean genero, int id)
			throws ClassNotFoundException, SQLException {
		boolean res;
		PreparedStatement pst = null;

		pst = VariablesComunes.conexion
				.prepareStatement(Consultas.CONSULTAACTUALIZACIONGENERO);
		pst.setString(1, genero.getClave());
		pst.setString(2, genero.getNombre());
		pst.setString(3, genero.getDescripcion());
		pst.setInt(4, id);

		res = pst.execute();

		return res;
	}

	@Override
	public boolean borrarRegistro(int id) throws ClassNotFoundException,
			SQLException {
		PreparedStatement pst = null;

		pst = VariablesComunes.conexion
				.prepareStatement(Consultas.CONSULTABORRADOGENEROS);
		pst.setInt(1, id);

		return pst.execute();
	}

}
