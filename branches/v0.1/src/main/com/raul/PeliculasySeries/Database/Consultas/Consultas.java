package com.raul.PeliculasySeries.Database.Consultas;

/**
 * Clase que se usa para tener todas las consultas del tipo SELECT, INSERT,
 * UPDATE, DELETE de la BD
 * 
 * @author Raul
 *
 */
public class Consultas {

	/* Consultas de todos los registros */
	public static final String CONSULTATODASPELICULAS = "SELECT\n peliculas.id as Id, peliculas.clave AS Clave, peliculas.nombre AS Nombre, tipos.nombre AS Tipo, formatos.nombre AS Formato, productoras.nombre AS Productora, peliculas.temporada AS Temporada, estados.nombre as Estado,generos.nombre as Genero,peliculas.sipnosis as Sipnosis FROM peliculas\nINNER JOIN productoras ON (peliculas.productoras = productoras.clave)\nINNER JOIN tipos ON (peliculas.tipo = tipos.clave)\nINNER JOIN formatos ON (peliculas.formato = formatos.clave)\nINNER JOIN estados ON (peliculas.estados = estados.clave)\nINNER JOIN generos ON (peliculas.generos = generos.clave)\nORDER BY peliculas.nombre;";
	public static final String CONSULTATODOSFORMATOS = "SELECT * FROM formatos ORDER BY nombre";
	public static final String CONSULTATODOSTIPOS = "SELECT * FROM tipos ORDER BY nombre";
	public static final String CONSULTATODOSESTADOS = "SELECT * FROM estados ORDER BY nombre";
	public static final String CONSULTATODOSGENEROS = "SELECT * FROM generos ORDER BY nombre";
	public static final String CONSULTATODASPRODUCTORAS = "SELECT * FROM productoras ORDER BY nombre";

	/* Consultas por registro */
	public static final String CONSULTAUNAPELICULADETALLE = "SELECT\n peliculas.id as Id, peliculas.clave AS Clave, peliculas.nombre AS Nombre, tipos.nombre AS Tipo, formatos.nombre AS Formato, productoras.nombre AS Productora, peliculas.temporada,peliculas.sipnosis AS Sipnosis, estados.nombre as Estado, generos.nombre as Genero FROM peliculas\nINNER JOIN productoras ON (peliculas.productoras = productoras.clave)\nINNER JOIN tipos ON (peliculas.tipo = tipos.clave)\nINNER JOIN formatos ON (peliculas.formato = formatos.clave)\nINNER JOIN estados ON (peliculas.estados = estados.clave)\nINNER JOIN generos ON (peliculas.generos = generos.clave)\n WHERE peliculas.id= ?";
	public static final String CONSULTAUNAPELICULAEDICION = "SELECT\n id as Id, clave AS Clave, nombre AS Nombre, tipo AS Tipo, formato AS Formato, productoras AS Productora, temporada ,sipnosis AS Sipnosis,estados as Estado,generos as Genero FROM peliculas WHERE peliculas.id= ?;";
	public static final String CONSULTAUNTIPO = "SELECT * FROM tipos WHERE id =?";
	public static final String CONSULTAUNAPRODUCTORA = "SELECT * FROM productoras WHERE id =?";
	public static final String CONSULTAUNFORMATO = "SELECT * FROM formatos WHERE id =?";
	public static final String CONSULTAUNGENERO = "SELECT * FROM generos  WHERE id =?";
	public static final String CONSULTAUNESTADO = "SELECT * FROM estados WHERE id =?";

	/* consultas de insercion */
	public static final String CONSULTAINSERCIONPELICULAS = "INSERT INTO peliculas (clave, nombre, formato, tipo, productoras,temporada,sipnosis,estados,generos) VALUES(?,?,?,?,?,?,?,?,?)";
	public static final String CONSULTAINSERCIONFORMATOS = "INSERT INTO formatos (clave, nombre,descripcion) VALUES(?,?,?)";
	public static final String CONSULTAINSERCIONPRODUCTORAS = "INSERT INTO productoras (clave, nombre,descripcion) VALUES(?,?,?)";
	public static final String CONSULTAINSERCIONTIPOS = "INSERT INTO tipos (clave, nombre,descripcion) VALUES(?,?,?)";
	public static final String CONSULTAINSERCIONGENERO = "INSERT INTO generos (clave, nombre,descripcion) VALUES(?,?,?)";
	public static final String CONSULTAINSERCIONESTADO = "INSERT INTO estados (clave, nombre,descripcion) VALUES(?,?,?)";

	/* Consultas de modificacion */
	public static final String CONSULTAACTUALIZACIONPELICULA = "UPDATE peliculas SET clave =?, nombre = ?, formato=?, tipo=?, productoras=?, temporada=?, sipnosis=?, estados=?, generos=?   WHERE id=? ";
	public static final String CONSULTAACTUALIZACIONCATEGORIA = "UPDATE tipos SET clave = ?, nombre = ?, descripcion = ? WHERE id = ?";
	public static final String CONSULTAACTUALIZACIONPRODUCTORA = "UPDATE productoras SET clave = ?, nombre = ?, descripcion = ? WHERE id = ?";
	public static final String CONSULTAACTUALIZACIONFORMATO = "UPDATE formatos SET clave = ?, nombre = ?, descripcion = ? WHERE id = ?";
	public static final String CONSULTAACTUALIZACIONGENERO = "UPDATE generos SET clave = ?, nombre = ?, descripcion = ? WHERE id = ?";
	public static final String CONSULTAACTUALIZACIONESTADO = "UPDATE estados SET clave = ?, nombre = ?, descripcion = ? WHERE id = ?";

	/* Consultas de borrado */
	public static final String CONSULTABORRADOGENEROS = "DELETE FROM generos WHERE id = ?";
	public static final String CONSULTABORRADOCATEGORIAS = "DELETE FROM tipos WHERE id = ?";
	public static final String CONSULTABORRADOESTADOS = "DELETE FROM estados WHERE id = ?";
	public static final String CONSULTABORRADOFORMATOS = "DELETE FROM formatos WHERE id = ?";
	public static final String CONSULTABORRADOPRODUCTORAS = "DELETE FROM productoras WHERE id = ?";
	public static final String CONSULTABORRADOPELICULAS = "DELETE FROM peliculas WHERE id = ?";

	/* Consultas de Busqueda */
	public static final String CONSULTABUSQUEDAPELICULAS = "SELECT * FROM peliculas WHERE nombre LIKE '%' ? '%'";
}