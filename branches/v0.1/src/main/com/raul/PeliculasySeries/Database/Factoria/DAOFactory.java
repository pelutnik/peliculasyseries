/*

 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.raul.PeliculasySeries.Database.Factoria;

import com.raul.PeliculasySeries.Database.interfaces.InterfaceCategoriasDAO;
import com.raul.PeliculasySeries.Database.interfaces.InterfaceEstadosDAO;
import com.raul.PeliculasySeries.Database.interfaces.InterfaceFormatosDAO;
import com.raul.PeliculasySeries.Database.interfaces.InterfaceGenerosDAO;
import com.raul.PeliculasySeries.Database.interfaces.InterfacePeliculasDAO;
import com.raul.PeliculasySeries.Database.interfaces.InterfaceProductorasDAO;

/*import com.ipartek.fila2.interfaces.InterfaceAlumnoDAO;
 import com.ipartek.fila2.interfaces.InterfaceTablasDAO;
 import com.ipartek.fila2.interfaces.InterfaceTiradaDAO;
 */
/**
 * Clase que gestiona los diferentes tipos de conexion de bases de datos,
 * ficheros...
 * 
 * @author Curso
 */
public abstract class DAOFactory {

	public static final int MYSQL = 1;
	public static final int SQLSERVER = 2;
	public static final int ORACLE = 3;
	public static final int ODBC = 4;
	public static final int TXTFILE = 5;

	public abstract InterfaceEstadosDAO getInterfaceEstadoDAO();

	public abstract InterfacePeliculasDAO getInterfacePeliculasDAO();

	public abstract InterfaceCategoriasDAO getInterfaceCategoriasDAO();

	public abstract InterfaceProductorasDAO getInterfaceProductorasDAO();

	public abstract InterfaceFormatosDAO getInterfaceFormatosDAO();

	public abstract InterfaceGenerosDAO getInterfaceGenerosDAO();

	// Los metodos que se vayan poniendo aqu� habr� que implementarlos
	// en MySqlDAOFactory, OracleFactory, SqlServerFactory,..... (En nuestro
	// caso s�lo MySQL)

	// Dependiendo de que valor que se le pase al constructor devolver� una
	// instancia
	// del tipo que sea. En nuestro caso s�lo est� implementada para MySQL

	public static DAOFactory getDAOFactory(int tipo) {

		switch (tipo) {
		case SQLSERVER:
			return new SQLServerFactory();
		}
		return null;
	}

}
