package com.raul.PeliculasySeries.Database.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.raul.PeliculasySeries.Beans.ProductorasBean;
import com.raul.PeliculasySeries.Database.Consultas.Consultas;
import com.raul.PeliculasySeries.Database.interfaces.InterfaceProductorasDAO;
import com.raul.PeliculasySeries.Variables.VariablesComunes;

public class SQLServerProductorasDAO implements InterfaceProductorasDAO {

	/**
	 * Obtenemos todos los datos de las peliculas para crear una tabla en la
	 * pagina principal
	 * 
	 * @return
	 * @throws SQLException
	 *             , ClassNotFoundException
	 */
	@Override
	public ArrayList<ProductorasBean> obtenerDatos() throws SQLException,
			ClassNotFoundException {
		ArrayList<ProductorasBean> res = new ArrayList<ProductorasBean>();

		Statement statement = VariablesComunes.conexion.createStatement();
		ResultSet rs = statement
				.executeQuery(Consultas.CONSULTATODASPRODUCTORAS);

		// System.out.println(rs.toString());
		// rs.first();
		while (rs.next()) {
			ProductorasBean productoras = new ProductorasBean(
					rs.getString("clave"), rs.getString("nombre"),
					rs.getInt("id"));
			// System.out.println(productoras.toString());

			res.add(productoras);
		}

		return res;
	}

	@Override
	public ProductorasBean consultaUnaProductora(int id)
			throws ClassNotFoundException, SQLException {
		ProductorasBean res = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		pst = VariablesComunes.conexion
				.prepareStatement(Consultas.CONSULTAUNAPRODUCTORA);
		pst.setInt(1, id);

		rs = pst.executeQuery();

		while (rs.next()) {
			res = new ProductorasBean(rs.getString("clave"),
					rs.getString("nombre"), rs.getInt("id"),
					rs.getString("descripcion"));
		}

		return res;
	}

	/**
	 * Inserta un registro en la BD
	 * 
	 * @param productora
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	@Override
	public boolean insertarProductora(ProductorasBean productora)
			throws ClassNotFoundException, SQLException {

		boolean res = false;
		PreparedStatement pst = null;

		pst = VariablesComunes.conexion
				.prepareStatement(Consultas.CONSULTAINSERCIONPRODUCTORAS);
		pst.setString(1, productora.getClave());
		pst.setString(2, productora.getNombre());
		pst.setString(3, productora.getDescripcion());

		res = pst.execute();

		res = true;

		pst.close();

		return res;

	}

	@Override
	public boolean actualizarRegistro(ProductorasBean productora, int id)
			throws ClassNotFoundException, SQLException {
		boolean res;
		PreparedStatement pst = null;

		pst = VariablesComunes.conexion
				.prepareStatement(Consultas.CONSULTAACTUALIZACIONPRODUCTORA);
		pst.setString(1, productora.getClave());
		pst.setString(2, productora.getNombre());
		pst.setString(3, productora.getDescripcion());
		pst.setInt(4, id);

		res = pst.execute();

		return res;
	}

	@Override
	public boolean borrarRegistro(int id) throws ClassNotFoundException,
			SQLException {
		PreparedStatement pst = null;

		pst = VariablesComunes.conexion
				.prepareStatement(Consultas.CONSULTABORRADOPRODUCTORAS);
		pst.setInt(1, id);

		return pst.execute();
	}

}
