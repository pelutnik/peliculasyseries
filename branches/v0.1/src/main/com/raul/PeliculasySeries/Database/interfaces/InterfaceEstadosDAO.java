package com.raul.PeliculasySeries.Database.interfaces;

import java.sql.SQLException;
import java.util.ArrayList;

import com.raul.PeliculasySeries.Beans.EstadosBean;

public interface InterfaceEstadosDAO {

	public ArrayList<EstadosBean> obtenerDAtos() throws ClassNotFoundException,
			SQLException;

	public EstadosBean consultaUnEstado(int id) throws ClassNotFoundException,
			SQLException;

	public boolean actualizarRegistro(EstadosBean estado, int id)
			throws ClassNotFoundException, SQLException;

	public boolean insertarEstado(EstadosBean estado)
			throws ClassNotFoundException, SQLException;

	public boolean borrarRegistro(int id) throws ClassNotFoundException,
			SQLException;
}
