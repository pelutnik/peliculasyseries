package com.raul.PeliculasySeries.Database.Factoria;

import java.sql.SQLException;

import com.raul.PeliculasySeries.Database.DAO.SQLServerCategoriasDAO;
import com.raul.PeliculasySeries.Database.DAO.SQLServerEstadosDAO;
import com.raul.PeliculasySeries.Database.DAO.SQLServerFormatosDAO;
import com.raul.PeliculasySeries.Database.DAO.SQLServerGenerosDAO;
import com.raul.PeliculasySeries.Database.DAO.SQLServerPeliculasDAO;
import com.raul.PeliculasySeries.Database.DAO.SQLServerProductorasDAO;
import com.raul.PeliculasySeries.Database.interfaces.InterfaceCategoriasDAO;
import com.raul.PeliculasySeries.Database.interfaces.InterfaceEstadosDAO;
import com.raul.PeliculasySeries.Database.interfaces.InterfaceFormatosDAO;
import com.raul.PeliculasySeries.Database.interfaces.InterfaceGenerosDAO;
import com.raul.PeliculasySeries.Database.interfaces.InterfacePeliculasDAO;
import com.raul.PeliculasySeries.Database.interfaces.InterfaceProductorasDAO;
import com.raul.PeliculasySeries.Utils.Utilidades;
import com.raul.PeliculasySeries.Variables.VariablesComunes;

public class SQLServerFactory extends DAOFactory {

	public SQLServerFactory() {
		try {
			if (VariablesComunes.conexion == null) {
				Utilidades.conectar();
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public InterfaceEstadosDAO getInterfaceEstadoDAO() {
		return new SQLServerEstadosDAO();

	}

	@Override
	public InterfacePeliculasDAO getInterfacePeliculasDAO() {
		return new SQLServerPeliculasDAO();
	}

	@Override
	public InterfaceCategoriasDAO getInterfaceCategoriasDAO() {
		return new SQLServerCategoriasDAO();
	}

	@Override
	public InterfaceProductorasDAO getInterfaceProductorasDAO() {
		return new SQLServerProductorasDAO();
	}

	@Override
	public InterfaceFormatosDAO getInterfaceFormatosDAO() {
		return new SQLServerFormatosDAO();
	}

	@Override
	public InterfaceGenerosDAO getInterfaceGenerosDAO() {
		return new SQLServerGenerosDAO();
	}
}
