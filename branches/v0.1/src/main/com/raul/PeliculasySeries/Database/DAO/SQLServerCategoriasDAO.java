package com.raul.PeliculasySeries.Database.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.raul.PeliculasySeries.Beans.TiposPeliculasBean;
import com.raul.PeliculasySeries.Database.Consultas.Consultas;
import com.raul.PeliculasySeries.Database.interfaces.InterfaceCategoriasDAO;
import com.raul.PeliculasySeries.Variables.VariablesComunes;

public class SQLServerCategoriasDAO implements InterfaceCategoriasDAO {

	/*
	 * private Connection conectar() throws ClassNotFoundException, SQLException
	 * { Connection conn = null; Class.forName(Constantes.DRIVER);
	 * 
	 * conn = DriverManager.getConnection(Constantes.URLSERVIDOR,
	 * Constantes.USER, Constantes.PASSWORD);
	 * 
	 * return conn; }
	 */

	/**
	 * Obtenemos todos los datos de las peliculas para crear una tabla en la
	 * pagina principal
	 * 
	 * @return
	 * @throws SQLException
	 */
	@Override
	public ArrayList<TiposPeliculasBean> obtenerDatos() throws SQLException,
			ClassNotFoundException {

		ArrayList<TiposPeliculasBean> res = new ArrayList<TiposPeliculasBean>();

		Statement statement = VariablesComunes.conexion.createStatement();
		ResultSet rs = statement.executeQuery(Consultas.CONSULTATODOSTIPOS);

		while (rs.next()) {
			TiposPeliculasBean tiposPelicula = new TiposPeliculasBean(
					rs.getString("clave"), rs.getString("nombre"),
					rs.getInt("id"));
			// System.out.println(tiposPelicula.toString());

			res.add(tiposPelicula);
		}

		return res;
	}

	/**
	 * Inserta un nuevo registro en la BD
	 * 
	 * @param tipo
	 * @return
	 */

	@Override
	public boolean insertarTipo(TiposPeliculasBean tipo)
			throws ClassNotFoundException, SQLException {

		boolean res = false;
		PreparedStatement pst = null;

		pst = VariablesComunes.conexion
				.prepareStatement(Consultas.CONSULTAINSERCIONTIPOS);
		pst.setString(1, tipo.getClave());
		pst.setString(2, tipo.getNombre());
		pst.setString(3, tipo.getDescripcion());

		res = pst.execute();

		res = true;

		pst.close();

		return res;

	}

	@Override
	public TiposPeliculasBean consultaUnTipoPelicula(int id)
			throws ClassNotFoundException, SQLException {

		TiposPeliculasBean res = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		pst = VariablesComunes.conexion
				.prepareStatement(Consultas.CONSULTAUNTIPO);
		pst.setInt(1, id);

		rs = pst.executeQuery();

		while (rs.next()) {
			res = new TiposPeliculasBean(rs.getInt("id"),
					rs.getString("clave"), rs.getString("nombre"),
					rs.getString("descripcion"));
		}

		return res;
	}

	@Override
	public boolean actualizarRegistro(TiposPeliculasBean tipo, int id)
			throws ClassNotFoundException, SQLException {

		boolean res;
		PreparedStatement pst = null;

		pst = VariablesComunes.conexion
				.prepareStatement(Consultas.CONSULTAACTUALIZACIONCATEGORIA);
		pst.setString(1, tipo.getClave());
		pst.setString(2, tipo.getNombre());
		pst.setString(3, tipo.getDescripcion());
		pst.setInt(4, id);

		res = pst.execute();

		return res;
	}

	@Override
	public boolean borrarRegistro(int id) throws ClassNotFoundException,
			SQLException {

		PreparedStatement pst = null;

		pst = VariablesComunes.conexion
				.prepareStatement(Consultas.CONSULTABORRADOCATEGORIAS);
		pst.setInt(1, id);

		return pst.execute();
	}

}
