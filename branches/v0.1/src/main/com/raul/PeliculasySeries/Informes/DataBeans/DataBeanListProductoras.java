package com.raul.PeliculasySeries.Informes.DataBeans;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.raul.PeliculasySeries.Beans.EstadisticasProductora;
import com.raul.PeliculasySeries.Variables.VariablesComunes;

public class DataBeanListProductoras {

	/*
	 * private static Connection conectar() throws ClassNotFoundException,
	 * SQLException { Connection conn = null; Class.forName(Constantes.DRIVER);
	 *
	 * conn = DriverManager.getConnection(Constantes.URLSERVIDOR,
	 * Constantes.USER, Constantes.PASSWORD);
	 *
	 * return conn; }
	 */
	public ArrayList getDataBeanList() {
		ArrayList dataBeanList = new ArrayList();

		/*
		 * Consulta para recuperar los datos de la bd
		 */

		String sql = "select productoras.nombre as Nombre, count(peliculas.clave) as Contador  from productoras join peliculas on peliculas.productoras = productoras.clave group by productoras.nombre";
		try {
			// Connection cnn = conectar();

			Statement statement = VariablesComunes.conexion.createStatement();
			ResultSet rs = statement.executeQuery(sql);

			// System.out.println(rs.toString());
			// rs.first();
			while (rs.next()) {
				dataBeanList.add(produce(rs.getString("Nombre"),
						rs.getInt("Contador")));
			}
		} catch (SQLException ex) {

		} finally {
			// cnn.close();
		}
		/*
		 * dataBeanList.add(produce("English", 58));
		 * dataBeanList.add(produce("SocialStudies", 68));
		 * dataBeanList.add(produce("Maths", 38));
		 * dataBeanList.add(produce("Hindi", 88));
		 * dataBeanList.add(produce("Scince", 78));
		 */
		return dataBeanList;
	}

	/*
	 * This method returns a DataBean object, with subjectName , and marks set
	 * in it.
	 */
	private static EstadisticasProductora produce(String productora,
			int numPelis) {
		EstadisticasProductora dataBean = new EstadisticasProductora();

		dataBean.setProductora(productora);
		dataBean.setNumPelis(numPelis);

		return dataBean;
	}
}
