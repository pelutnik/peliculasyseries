<link href="/PeliculasySeries/css/reset.css" rel="stylesheet" />
<link href="/PeliculasySeries/css/normalize.css" rel="stylesheet" />
<!-- <link href="/PeliculasySeries/css/metro-bootstrap.min.css" rel="stylesheet" />
<link href="/PeliculasySeries/css/metro-bootstrap-responsive.min.css" rel="stylesheet" />
<link href="/PeliculasySeries/css/iconFont.css" rel="stylesheet" />-->
<!--<link href="/PeliculasySeries/css/bootstrap-select.css" rel="stylesheet" />-->
<link href="/PeliculasySeries/css/bootstrap-dialog.css" rel="stylesheet" />
<link href="/PeliculasySeries/css/jquery.dataTables.css" rel="stylesheet">

<link href="/PeliculasySeries/css/style.css" rel="stylesheet" />

<link href="/PeliculasySeries/css/bootstrap.css" rel="stylesheet" />

<!-- Css Notificaciones -->
<link href="/PeliculasySeries/cssNotificaciones/pnotify.core.css" rel="stylesheet" />
<link href="/PeliculasySeries/cssNotificaciones/pnotify.buttons.css" rel="stylesheet" />
<link href="/PeliculasySeries/cssNotificaciones/pnotify.picon.css" rel="stylesheet" />
<link href="/PeliculasySeries/cssNotificaciones/pnotify.history.css" rel="stylesheet" />
<link href="/PeliculasySeries/jsNotificaciones/includes/font-awesome-4.0.3/css/font-awesome.css" rel="stylesheet" />
<link href="/PeliculasySeries/jsNotificaciones/oxygen/icons.css" rel="stylesheet" />
<link href="/PeliculasySeries/jsNotificaciones/includes/pform.css" rel="stylesheet" />
<link href="/PeliculasySeries/jsNotificaciones/includes/pform-bootstrap.css" rel="stylesheet" />
<link href="/PeliculasySeries/jsNotificaciones/includes/bootstrap3/css/bootstrap.css" id="bootstrap-css" rel="stylesheet" />
<link href="/PeliculasySeries/cssNotificaciones/style.css" rel="stylesheet" />
<!-- Fin Css Notificaciones -->

<!-- Material Design -->
<link href="/PeliculasySeries/css/material.css" rel="stylesheet" />
<link href="/PeliculasySeries/css/material-wfont.css" rel="stylesheet" />
<link href="/PeliculasySeries/css/ripples.css" rel="stylesheet" />
<!-- Fin Material Design -->

<link href="/PeliculasySeries/cssPDFViewer/bootstrap-pdf-viewer.css" rel="stylesheet" />

<!-- JQuery -->
<script src="/PeliculasySeries/jsJQuery/jquery-2.1.1.js"></script>
<script src="/PeliculasySeries/jsJQuery/jquery-ui.min.js"></script>
<!--  script src="/PeliculasySeries/jsJQuery/jquery.searchabledropdown-1.0.8.min.js"></script>-->
<!-- Fin JQuery -->

<!-- Otras librerias -->
<script src="/PeliculasySeries/js/jquery.dataTables.js"></script>
<script src="/PeliculasySeries/js/MisScripts/notificaciones.js"></script>
<!-- Fin Otras librerias -->

<!-- Bootstrap -->
<script src="/PeliculasySeries/jsBootstrap/bootstrap.min.js"></script>
<!-- <script src="/PeliculasySeries/jsBootstrap/bootstrap-select.js"></script> -->
<script src="/PeliculasySeries/jsBootstrap/bootstrap-dialog.js"></script>
<!-- Fin Bootstrap -->

<!-- Material Design -->
<script src="/PeliculasySeries/jsMaterial/material.js"></script>
<script src="/PeliculasySeries/jsMaterial/ripples.js"></script>
<!-- Fin Material Design -->

<!-- Mis Scripts -->
<script src="/PeliculasySeries/js/utilidades.js"></script>
<script src="/PeliculasySeries/js/MisScripts/funcionesguardado.js"></script>
<!-- Fin Mis Scripts -->


<!-- Notificaciones -->
<script src="/PeliculasySeries/jsNotificaciones/pnotify.core.js"></script>
<script src="/PeliculasySeries/jsNotificaciones/pnotify.buttons.js"></script>
<script src="/PeliculasySeries/jsNotificaciones/pnotify.tooltip.js"></script>
<script src="/PeliculasySeries/jsNotificaciones/pnotify.nonblock.js"></script>
<script src="/PeliculasySeries/jsNotificaciones/pnotify.reference.js"></script>
<script src="/PeliculasySeries/jsNotificaciones/pnotify.desktop.js"></script>
<script src="/PeliculasySeries/jsNotificaciones/pnotify.callbacks.js"></script>
<script src="/PeliculasySeries/jsNotificaciones/pnotify.confirm.js"></script>
<script src="/PeliculasySeries/jsNotificaciones/pnotify.mobile.js"></script>
<script src="/PeliculasySeries/jsNotificaciones/pnotify.history.js"></script>
<script src="/PeliculasySeries/jsNotificaciones/includes/beautify.js"></script>
 <!--Fin Notificaciones -->

<script src="/PeliculasySeries/js/bootbox.js"></script>

<script src="/PeliculasySeries/jsPDFViewer/pdf.js"></script>
<script src="/PeliculasySeries/jsPDFViewer/bootstrap-pdf-viewer.js"></script>
<!-- <script src="/PeliculasySeries/js/jquery-2.1.1.js"></script>-->

<!-- <script src="/PeliculasySeries/js/noty/packaged/jquery.noty.packaged.js"></script>
<script src="/PeliculasySeries/js/jquery.widget.min.js"></script>
<script src="/PeliculasySeries/js/jquery.mousewheel.js"></script>
<script src="/PeliculasySeries/js/jquery.dataTables.js"></script>
<script src="/PeliculasySeries/js/jquery-ui.js"></script>
<script src="/PeliculasySeries/js/load-metro.js"></script>
<script src="/PeliculasySeries/js/metro-times.js"></script>






<script src="/PeliculasySeries/js/bootstrap.min.js"></script>-->



