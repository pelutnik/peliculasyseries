<div class="well bs-component" style="margin: 10px 30px 0px;">

	<ul class="nav nav-tabs">
		<li><a data-toggle="tab" href="#principal" class="active">Principal</a></li>
		<li><a data-toggle="tab" href="#nueCat">Nueva Categoria</a></li>
		<li><a data-toggle="tab" href="#nueEst">Nuevo Estado</a></li>
		<li><a data-toggle="tab" href="#nueFor">Nuevo Formato</a></li>
		<li><a data-toggle="tab" href="#nueGen">Nuevo Genero</a></li>
		<li><a data-toggle="tab" href="#nuePro">Nueva Productora</a></li>
	</ul>
	<div id="myTabContent" class="tab-content">
		<div id="principal" class="tab-pane fade active in">
			<h2>Principal</h2>
			
			<form id="formPrincipal" class="form-horizontal">
    			<fieldset>
	    			<div class="form-group">
	    				<label for="clave" class="col-lg-2 control-label">Clave</label>
	    				<div class="col-lg-10">
	    					<input id="clave" name="clave" type="text" value="" class="form-control" />
	    				</div>
                    </div>
	    			
	    			<div class="form-group">
		    			<label for="nombre" class="col-lg-2 control-label">Nombre</label>
		    			<div class="col-lg-10">
		    				<input id="nombre" name="titulo" type="text" value="" class="form-control" />
		    			</div>
	    			</div>
	    			
	    			<div class="form-group">
		    			<label for="formatos" class="col-lg-2 control-label">Formatos</label>	    			
		    			<!-- Listado de formatos -->
		    			<div class="col-lg-10">
			    			<select name="formato" id="formatlist" class="form-control selectpicker" data-live-search="true">
			    			</select>
		    			</div>   			
	    			</div>
	    			
	    			<div class="form-group">
	    				<label class="col-lg-2 control-label" >Categorias</label>
	    				<!-- Listado de tipos -->
		    			<div class="col-lg-10">
		    				<!--<a class="col-lg-2 control-label">Categorias</a>-->
		    				<select name="tipo" id="tipolist" class="form-control selectpicker" data-live-search="true">
		    				</select>
		    				
		    			</div>
	    			</div>
	    			
	    			<div class="form-group">
		                <label for="productora" class="col-lg-2 control-label">Productoras</label>
		    			<!-- Listado de productoras -->
			    		<div class="col-lg-10">
			    			<select name="productora" id="produlist" class="form-control selectpicker" data-live-search="true">
			    			</select>
		    			</div>
	    			</div>
	    			
	    			<div class="form-group">
		    			<label for="estados" class="col-lg-2 control-label">Estados</label>
		    			<!-- Listado de estados -->
			    		<div class="col-lg-10">
			    			<select name="estado" id="estadoslist" class="form-control selectpicker" data-live-search="true">
			    			</select>
		    			</div>
	    			</div>
	    			
	    			<div class="form-group">
		    			<label for="generos" class="col-lg-2 control-label">Generos</label>
		    			<!-- Listado de productoras -->
			    		<div class="col-lg-10">
			    			<select name="genero" id="generoslist" class="form-control selectpicker" data-live-search="true">
			    			</select>
		    			</div>
	    			</div>
	    			
	    			<div class="form-group">
		    			<label for="temporada" class="col-lg-2 control-label">Temporada</label>
		    			<div class="col-lg-10">
	                        <input name="temporada" id="temporada" type="number" min=0 class="form-control" />
	                    </div>
                    </div>
                    
                    <div class="form-group">
		    			<label for="sipnosis" class="col-lg-2 control-label">Sipnosis</label>
		    			<div class="col-lg-10">
	                        <textarea rows="5" class="form-control" name="sipnosis" id="sipnosis" placeholder="Sipnosis de la pelicula o serie"></textarea>
	                    </div>
                    </div>
    			</fieldset>
    		</form>
    		<div class="btn-group">
	     		<a class="btn btn-primary" id="new">Nuevo</a>
		    </div>
		    
		    <div class="btn-group">
		     	<a class="btn btn-primary" id="save">Guardar</a>
		     </div>
		</div>
		
		
		<div id="nueCat" class="tab-pane fade">
			<h2>Nueva Categoria</h2>
			
		     
			<form id="formCategorias">
				<fieldset>
					<div class="form-group">
						<label class="col-lg-2 control-label">Clave</label>
						<div class="col-lg-10">
							<input type="text" id="claveCat" class="form-control"/>
						</div>	
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">Nombre</label>
						<div class="col-lg-10">
							<input type="text" id="nombreCat" class="form-control"/>
						</div>	
					</div>
				</fieldset>
			</form>
			<div class="btn-group">
	     		<a class="btn btn-primary" id="new">Nuevo</a>
		    </div>
		    
		    <div class="btn-group">
		     	<a class="btn btn-primary" id="save">Guardar</a>
		     </div>
		</div>
		<div id="nueEst" class="tab-pane fade">
			<h2>Nuevo Estado</h2>
			
		     
			<form id="formEstados">
				<fieldset>
					<div class="form-group">
						<label class="col-lg-2 control-label">Clave</label>
						<div class="col-lg-10">
							<input type="text" id="claveEst" class="form-control"/>
						</div>	
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">Nombre</label>
						<div class="col-lg-10">
							<input type="text" id="nombreEst" class="form-control"/>
						</div>	
					</div>
				</fieldset>
			</form>
			<div class="btn-group">
	     		<a class="btn btn-primary" id="new">Nuevo</a>
		    </div>
		    
		    <div class="btn-group">
		     	<a class="btn btn-primary" id="save">Guardar</a>
		     </div>
			
		</div>
		<div id="nueFor" class="tab-pane fade">
			<h2>Nuevo Formato</h2>
			
		     
			<form id="formFormatos">
				<fieldset>
					<div class="form-group">
						<label class="col-lg-2 control-label">Clave</label>
						<div class="col-lg-10">
							<input type="text" id="claveFor" class="form-control"/>
						</div>	
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">Nombre</label>
						<div class="col-lg-10">
							<input type="text" id="nombreFor" class="form-control"/>
						</div>	
					</div>
				</fieldset>
			</form>
			<div class="btn-group">
	     		<a class="btn btn-primary" id="new">Nuevo</a>
		    </div>
		    
		    <div class="btn-group">
		     	<a class="btn btn-primary" id="save">Guardar</a>
		     </div>
		</div>
		<div id="nueGen" class="tab-pane fade">
			<h2>Nuevo Genero</h2>
			
		     
			<form id="formGeneros">
				<fieldset>
					<div class="form-group">
						<label class="col-lg-2 control-label">Clave</label>
						<div class="col-lg-10">
							<input type="text" id="claveGen" class="form-control"/>
						</div>	
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">Nombre</label>
						<div class="col-lg-10">
							<input type="text" id="nombreGen" class="form-control"/>
						</div>	
					</div>
				</fieldset>
			</form>
			<div class="btn-group">
	     		<a class="btn btn-primary" id="new">Nuevo</a>
		    </div>
		    
		    <div class="btn-group">
		     	<a class="btn btn-primary" id="save">Guardar</a>
		     </div></div>
		<div id="nuePro" class="tab-pane fade">
			<h2>Nueva Productora</h2>
			
		     
			<form id="formProductoras">
				<fieldset>
					<div class="form-group">
						<label class="col-lg-2 control-label">Clave</label>
						<div class="col-lg-10">
							<input type="text" id="clavePro" class="form-control"/>
						</div>	
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">Nombre</label>
						<div class="col-lg-10">
							<input type="text" id="nombrePro" class="form-control"/>
						</div>	
					</div>
				</fieldset>
			</form>
			<div class="btn-group">
	     		<a class="btn btn-primary" id="new">Nuevo</a>
		    </div>
		    
		    <div class="btn-group">
		     	<a class="btn btn-primary" id="save">Guardar</a>
		     </div></div>
	</div>
    </div>
	<!-- Menu de opciones del formulario
	<div style="margin: 10px 25px 0px;">
		 
	     
	     <div class="btn-group">
	     	<a class="btn btn-primary" id="btnTipo">Nueva categoria</a>
	     </div>
	     
	     <div class="btn-group">
	     	<a class="btn btn-primary" id="btnEstado">Nuevo estado</a>
	     </div>
	     
	     <div class="btn-group">
	     	<a class="btn btn-primary" id="btnFormato" href="javascript:void(0)">Nuevo formato</a>  
	     </div>
	     
	      <div class="btn-group">
	     	<a class="btn btn-primary" id="btnGenero">Nuevo genero</a>	       	
	     </div>
	        
	     <div class="btn-group">
	     	<a class="btn btn-primary" id="btnProductora">Nueva productora</a>
	       
     	</div>

     </div>
	Fin del menu de opciones del formulario -->
 </div>
    	
    	