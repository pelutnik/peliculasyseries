<!-- Inicio de menu -->

	<div class="navbar navbar-info">
		<div class="navbar-header">
			 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand" href="/PeliculasySeries/">Home</a>
		</div>
		<div class="navbar-collapse collapse navbar-responsive-collapse">
			<ul class="nav navbar-nav">
				<li class="dropdown">
		            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Archivo <b class="caret"></b></a>
		            <ul class="dropdown-menu">
			            <li><a href="/PeliculasySeries/nuevo.jsp">Nueva Pelicula</a></li>
			            <li class="divider"></li> 
			            <li><a href="/PeliculasySeries/auxiliares/nuevo.jsp?seccion=tipo">Nueva Categoria</a>
			            <li><a href="/PeliculasySeries/auxiliares/nuevo.jsp?seccion=estado">Nuevo Estado</a>
			            <li><a href="/PeliculasySeries/auxiliares/nuevo.jsp?seccion=formatos">Nuevo Formato</a>
			            <li><a href="/PeliculasySeries/auxiliares/nuevo.jsp?seccion=genero">Nuevo Genero</a>
			            <li><a href="/PeliculasySeries/auxiliares/nuevo.jsp?seccion=productora">Nueva Productora</a>
		            </ul>
	            </li>
	            <li class="dropdown">
		            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Auxiliares <b class="caret"></b></a>
		            <ul class="dropdown-menu">
			            <li><a href="/PeliculasySeries/auxiliares/tipos.jsp">Categorias</a></li>
                        <li><a href="/PeliculasySeries/auxiliares/estados.jsp">Estados</a></li> 
                        <li><a href="/PeliculasySeries/auxiliares/formatos.jsp">Formatos</a></li>
                         <li><a href="/PeliculasySeries/auxiliares/generos.jsp">Generos</a></li>
                        <li><a href="/PeliculasySeries/auxiliares/productoras.jsp">Productoras</a></li> 
		            </ul>
	            </li>
	            <li class="dropdown">
		            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Informes <b class="caret"></b></a>
		            <ul class="dropdown-menu">
			            <li><a href="/PeliculasySeries/informes/general.jsp">General</a></li>
                            <li><a href="/PeliculasySeries/informes/portipo.jsp">Por categoria</a></li>
                            <li><a href="/PeliculasySeries/informes/porestado.jsp">Por estado</a></li>
                            <li><a href="/PeliculasySeries/informes/porformato.jsp">Por formato</a></li>
                             <li><a href="/PeliculasySeries/informes/porgenero.jsp">Por genero</a></li>
                             <li><a href="/PeliculasySeries/informes/porproductora.jsp">Por productora</a></li>
                            <li>
                            	<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Estadisticas</a>
                            	<ul class="dropdown-menu">
                            		<li>
                            			<a href="/PeliculasySeries/informes/estadisticaproductoras.jsp">por productora</a>
                            		</li>
                            	</ul>
                            </li>
                        </ul>
	            	</li>
	            <li class="dropdown">
		            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Envio Emails<b class="caret"></b></a>
		            <ul class="dropdown-menu">
			            
	                		<li>
	                			<a href="/PeliculasySeries/email/envioinformesmail.jsp">Envio Informes</a>
	                		</li>
	                		<li>
	                			 <a href="/PeliculasySeries/email/configuracionmail.jsp">Configuración</a>
	                		</li>
	                	
                        </ul>
	            	</li>
			</ul>
			<form class="navbar-form navbar-right" id="bloodhound">
        		<input type="text" class="form-control col-lg-8 typeahead" id="buscador" placeholder="Search">
        	</form>
		</div>
	</div>
    <!-- Fin de menu -->