<!-- Inicio de menu -->
        <nav class="navigation-bar light">
            <div class="navigation-bar-content">
                <a class="element" href="/PeliculasySeries"><span class="icon-grid-view"> </span>HOME</a>
                <span class="element-divider"></span>
                <a class="pull-menu" href="#"></a>
                <ul class="element-menu">
                    <li>
                        <a href="#" class="dropdown-toggle">Archivo</a>
                        <ul class="dropdown-menu inverse" data-role="dropdown">
                            <li><a href="/PeliculasySeries/nuevo.jsp">Nueva Pelicula</a></li> 
                            <li><a href="/PeliculasySeries/auxiliares/nuevo.jsp?seccion=tipo">Nueva Categoria</a>
                            <li><a href="/PeliculasySeries/auxiliares/nuevo.jsp?seccion=estado">Nuevo Estado</a>
                            <li><a href="/PeliculasySeries/auxiliares/nuevo.jsp?seccion=formatos">Nuevo Formato</a>
                            <li><a href="/PeliculasySeries/auxiliares/nuevo.jsp?seccion=genero">Nuevo Genero</a>
                            <li><a href="/PeliculasySeries/auxiliares/nuevo.jsp?seccion=productora">Nueva Productora</a>
                        </ul>
                    </li>
                   	<span class="element-divider"></span>
                    <a class="pull-menu" href="#"></a>
                    <li>
                    <a href="#" class="dropdown-toggle">Auxiliares</a>
                        <ul class="dropdown-menu inverse" data-role="dropdown">
                            <li><a href="/PeliculasySeries/auxiliares/tipos.jsp">Categorias</a></li>
                            <li><a href="/PeliculasySeries/auxiliares/estados.jsp">Estados</a></li> 
                            <li><a href="/PeliculasySeries/auxiliares/generos.jsp">Generos</a></li> 
                            <li><a href="/PeliculasySeries/auxiliares/formatos.jsp">Formatos</a></li>
                            <li><a href="/PeliculasySeries/auxiliares/productoras.jsp">Productoras</a></li> 
                        </ul>
                    </li>
                    <span class="element-divider"></span>
                    <a class="pull-menu" href="#"></a>
                    <li>
                        <a href="#" class="dropdown-toggle">Informes</a>
                        <ul class="dropdown-menu inverse" data-role="dropdown">
                            <li><a href="/PeliculasySeries/informes/general.jsp">General</a></li>
                            <li><a href="/PeliculasySeries/informes/porproductora.jsp">Por productora</a></li>
                            <li><a href="/PeliculasySeries/informes/portipo.jsp">Por categoria</a></li>
                            <li><a href="/PeliculasySeries/informes/estado.jsp">Por estado</a></li>
                            <li><a href="/PeliculasySeries/informes/porformato.jsp">Por formato</a></li>
                            <li><a href="#" class="dropdown-toggle">Estadisticas</a>
                            	<ul class="dropdown-menu inverse" data-role="dropdown">
                            		<li><a href="/PeliculasySeries/informes/estadisticaproductoras.jsp">por productora</a></li>
                            	</ul>
                            </li>
                        </ul>
                    </li>
                </ul>
                <span class="element-divider"></span>
                <ul class="element-menu">
	                <li>
	                	<a href="#" class="dropdown-toggle">Envio Emails</a>
	                	<ul class="dropdown-menu inverse" data-role="dropdown">
	                		<li>
	                			<a href="/PeliculasySeries/email/envioinformesmail.jsp">Envio Informes</a>
	                		</li>
	                		<li>
	                			 <a href="/PeliculasySeries/email/configuracionmail.jsp">Configuración</a>
	                		</li>
	                	</ul>
	                </li>
	            </ul>
                <a class="pull-menu" href="#"></a>
                <!-- <span class="element-divider"></span>
                <a href="/PeliculasySeries/swagger.jsp" class="element">Swagger</a>
                <a class="pull-menu" href="#"></a>-->
            </div>
        </nav>
        <!-- Fin de menu -->