        <div class="fluent-menu" data-role="fluentmenu">
            <ul class="tabs-holder">
                <li class="active"><a href="#tab_home">Home</a></li>
            </ul>
            
            <div class="tabs-content success">
                <div class="tab-panel" id="tab_home">
                    <div class="tab-panel-group">
                    	<button class="fluent-big-button" id="new"><span class="icon-new"></span>Nuevo</button>
                        <button class="fluent-big-button" id="save"><span class="icon-floppy"></span>Guardar</button>
                        <button class="fluent-big-button" id="reset"><span class="icon-delicious"></span>Resetear<br />formulario</button>
                    </div>
                    <div class="tab-panel-group">
                        <button class="fluent-big-button large" id="btnFormato"><span class="icon-tablet"></span>Nuevo<br />formato</button>
                        <button class="fluent-big-button" id="btnTipo"><span class="icon-tablet"></span>Nuevo<br />tipo</button>
                        <button class="fluent-big-button" id="btnEstado"><span class="icon-tablet"></span>Nuevo<br />Estado</button>
                        <button class="fluent-big-button" id="btnGenero"><span class="icon-tablet"></span>Nuevo<br />Genero</button>
                        <button class="fluent-big-button" id="btnProductora"><span class="icon-tablet"></span>Nuevo<br />productora</button>
                    </div>
                    <div class="tab-panel-group">
                    	<button class="fluent-big-button large" id="tipos"><span class="icon-search"></span>Mostrar<br />tipos</button>
                    	<button class="fluent-big-button large" id="formatos"><span class="icon-search"></span>Mostrar<br />formatos</button>
                        <button class="fluent-big-button large" id="productoras"><span class="icon-search"></span>Mostrar<br />productoras</button>
                        <button class="fluent-big-button large" id="genero"><span class="icon-search"></span>Mostrar<br />genero</button>
                        <button class="fluent-big-button large" id="estado"><span class="icon-search"></span>Mostrar<br />estado</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fin de Tabs -->