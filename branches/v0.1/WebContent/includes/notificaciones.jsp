
<link href="pnotify.core.css" rel="stylesheet" />
<link href="pnotify.buttons.css" rel="stylesheet" />
<link href="pnotify.picon.css" rel="stylesheet" />
<link href="pnotify.history.css" rel="stylesheet" />
<link href="includes/font-awesome-4.0.3/css/font-awesome.css" rel="stylesheet" />
<link href="oxygen/icons.css" rel="stylesheet" />
<link href="includes/pform.css" rel="stylesheet" />
<link href="includes/pform-bootstrap.css" rel="stylesheet" />
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" rel="stylesheet" />
<link href="includes/bootstrap3/css/bootstrap.css" id="bootstrap-css" rel="stylesheet" type="text/css" />
<style>
	.ui-pnotify.stack-bar-top {
		right: 0;
		top: 0;
	}
</style>





<script src="pnotify.core.js"></script>
<script src="pnotify.buttons.js"></script>
<script src="pnotify.tooltip.js"></script>
<script src="pnotify.nonblock.js"></script>
<script src="pnotify.reference.js"></script>
<script src="pnotify.desktop.js"></script>
<script src="pnotify.callbacks.js"></script>
<script src="pnotify.confirm.js"></script>
<script src="pnotify.mobile.js"></script>
<script src="pnotify.history.js"></script>
<script src="includes/beautify.js"></script>