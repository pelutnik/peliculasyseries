<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf8">
		<title>Configuración del correo electronico</title>
		<%@ include file="../includes/head.jsp" %>
	</head>
	<body class="metro">
		<h1>Configuración del correo electronico</h1>
		<h2>Configuración del correo electronico para enviar los informes de las peliculas y series</h2>
		<%@ include file="../includes/menu.jsp" %>
		
		<div>
			<form>
				<fieldset>
					<legend>Configuracion de las propiedades del email para enviar correos</legend>
					<label>Servidor saliente de correo (SMTP)</label>
					<div class="input-control text" data-role="input-control">
						<input type="text" id="smtpHost" name="smtpHost"  />
						<button class="btn-clear" tabindex="-1" type="button"></button>
					</div>
					
					<label>Puerto del servidor saliente de correo (SMTP)</label>
					<div class="input-control text" data-role="input-control">
						<input type="text" id="smtpPort" name="smtpPort" />
						<button class="btn-clear" tabindex="-1" type="button"></button>
					</div>
					
					<label>Usuario del correo electronico</label>
					<div class="input-control text" data-role="input-control">
						<input type="text" id="mailUser" name="mailUser" />
						<button class="btn-clear" tabindex="-1" type="button"></button>
					</div>
					
					<label>Password del correo electronico</label>
					<div class="input-control text" data-role="input-control">
						<input type="text" id="mailPass" name="mailPass" />
						<button class="btn-clear" tabindex="-1" type="button"></button>
					</div>
					
					<label>Tu Email</label>
					<div class="input-control text" data-role="input-control">
						<input type="text" id="mailSender" name="mailSender" />
						<button class="btn-clear" tabindex="-1" type="button"></button>
					</div>
					
					<label>Nombre que quieres que aparezca para enviar el Email</label>
					<div class="input-control text" data-role="input-control">
						<input type="text" id="mailName" name="mailName" />
						<button class="btn-clear" tabindex="-1" type="button"></button>
					</div>
				</fieldset>
			</form>
		</div>
	</body>
</html>