<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf8">
		<title>Enviar Informes por email</title>
		<%@ include file="../includes/head.jsp" %>
	</head>
	<body>
		<h1>Envio de informes por e-Mail</h1>
		<%@ include file="../includes/menu.jsp" %>
		<div class="well bs-component" style="margin: 10px 30px 0px;">
		<form id="principal">
			<fieldset>
				<legend>
					<h2>Envio de informes por email</h2>
				</legend>
                
                <label class="control-label">Paso 1.- Elegir el tipo de informe que queremos enviar por correo electronico</label>
				<div class="form-group">
					
					<div class="col-lg-10">
						<div class="radio radio-primary">
						    <label>
						        <input type="radio" checked="checked" name="rinformes" id="general" value="general" />
						      	General
						    </label>
						</div>
					</div>
				
				
					<div class="col-lg-10">
	                    <div class="radio radio-primary">
	                        <label>
	                            <input type="radio" name="rinformes" id="porCategoria" value="porcategoria" />
	                            Por categoria
	                        </label>
	                    </div>
	                </div>
	                
	                <div class="col-lg-10">
						<div class="radio radio-primary">
						    <label>
						        <input type="radio" name="rinformes" id="porProductora" value="porproductora" />
						        Por Productora
						    </label>
						</div>
					</div>
					
					<div class="col-lg-10">
						<div class="radio radio-primary">
						    <label>
						        <input type="radio" name="rinformes" id="porFormato" value="porformato" />
						        Por Formato
						    </label>
						</div>
					</div>
				</div>
				<label class="control-label">Paso 2.- Introducir los datos de la cuenta de correo a la que queremos enviarlo</label>
				<div class="form-group">
					<label for="email" class="col-lg-2 control-label">Introducir email a enviar</label>
					<div class="col-lg-10">
						<input type="text" placeholder="Correo electronico" name="email" id="email" class="form-control" >
					</div>		
				</div>
				
				<label class="control-label">Paso 3.- Introducir mensaje a enviar</label>
				<div class="form-group">
					<label for="mensaje" class="col-lg-2 control-label">Mensaje a enviar</label>
					<div class="col-lg-10">
						<textarea placeholder="Mensaje" name="mensaje" id="mensaje" class="form-control" ></textarea>
					</div>
				</div>
				<div class="input-control textarea" data-role="input-control">
					<input type="text" placeholder="Correo electronico" name="archivoEnviar" id="archivoEnviar" readonly="readonly" >
				</div>
			</fieldset>
		</form>
		<button id="enviar">Envio del informe</button>
		</div>
		
		<script>

			$(document).ready(function(){
				$.material.init();
			
			var checkeado = 'general';

			$("input[name=rinformes]:radio").change(function(){
				checkeado = $("input[name=rinformes]:checked").val();

			});
			$("#enviar").click(function(){
				var url ='';
				var informe ='';
				// Paso 1 seleccionar que tipo de informe queremos enviar
				switch(checkeado){
				case "general":
					url = '../server/pelisseries/informe';
					break;
				case "porproductora":
					url = '../server/productoras/informe';
					break;
				case "porcategoria":
					url = '../server/informes/portipo';
					break;
				case "porformato":
					url = '../server/informes/porformato';
					break;
				}

				$.ajax({
					url			:	url,
					type		: 	'get',
					async		:	false,
					success		:	function(data){
						$("#archivoEnviar").val(data.mensaje.replace('../', ''));
					},
					error 		: 	function(xhr, status, error) {
						console.error("Error en la petición AJAX de los datos de matrículas: ", error);
					}
				});

				// Paso 2 llamar a las funciones de envio del informe
				var datos = JSON.stringify(getFormData($('#principal')));
				
				$.ajax({
					url			:	"../server/envioemail",
					type		: 	'post',
					dataType	:	'json', 
			        contentType	:	'application/json; charset=utf-8',
					async		:	false,
					data		: datos,
					success		:	function(data){
						alert(data)
					},
					error 		: 	function(xhr, status, error) {
						console.error("Error en la petición AJAX de los datos de matrículas: ", error);
					}
				});
				
			});
			});
		</script>
	</body>
</html>