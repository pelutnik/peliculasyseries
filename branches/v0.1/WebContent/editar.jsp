<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf8">
        <title class="titulo">Editar elemento... </title>
		<%@ include file="includes/head.jsp" %>
		
		<script src="js/noty/packaged/jquery.noty.packaged.js"></script>
		
		 <!--A partir de aqui van mis funciones de script 	-->
		<script src="js/MisScripts/notificaciones.js"></script>
		<script src="js/MisScripts/formulariosPequenos.js"></script>
		<script src="js/MisScripts/funcionesguardado.js"></script>
		
		<script src="js/cargaTablasBusqueda.js"></script>
    </head>
    <body>
    	<h1>Edicion de peliculas </h1>
    	<%@ include file="includes/menu.jsp" %>
    	<h2 class="titulo">Editar elemento</h2>
    	<%@ include file="includes/formularioPeliculas.jsp" %>

    	<script>
	    	$(document).ready(function(){
	    		recuperarElementos(cargarFormatos(),'formatlist');
	    		recuperarElementos(cargarTipos(),'tipolist');
	    		recuperarElementos(cargarProductoras(),'produlist');
	    		recuperarElementos(cargarEstados(),'estadoslist');
				recuperarElementos(cargarGeneros(), 'generoslist');
				
	    		id = $.urlParam('id');
				cargarPelicula(id,true);
	
				$("#new").attr("disabled", "disabled");

				//$("select").searchable();
	    	});
	        	function cargarElemento(peli){
	            	$("#clave").val(peli.clave);
	            	$("#clave").attr("readonly", "readonly");
	            	$("#nombre").val(peli.titulo);
	            	$("#nombre").attr("readonly", "readonly");
	            	$("#formatlist").val(peli.formato);
	            	$("#tipolist").val(peli.tipo);
	            	$("#produlist").val(peli.productora);
	            	$("#temporada").val(peli.temporada);           
	            	$("#sipnosis").val(peli.sipnosis);
	            	$("#estadoslist").val(peli.estado); 
	            	$("#generoslist").val(peli.genero);  
	            	$(".titulo").html("Editar elemento... " + peli.titulo);        	            	
	            }
	                 	
	            /*Crear un JSON para almacenar la pelicula introducida*/
	            $("#save").click(function(){

	                //alert(getFormData($('#formPrincipal')));
	            	var datos = JSON.stringify(getFormData($('#formPrincipal')));
	            	
	                var urlModificar = 'server/pelisseries/actualizar?id=' + id;
	                if (actualizar(urlModificar, datos)){
	                	var url = "detalles.jsp?id=" + id;    
    					$(location).attr('href',url);
	                }
	             });

    	</script>
    	<script src="js/MisScripts/tablasAyuda.js"></script>
    </body>
</html>