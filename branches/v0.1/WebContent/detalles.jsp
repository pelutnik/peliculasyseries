<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf8">
		
		<title class="titulo">Pagina de detalles</title>
		
		<%@ include file="includes/head.jsp" %>
		
		<style type="text/css">
			.clear{
				border: 0 none;
			    clear: both;
			    font-size: 0;
			    height: 0;
			}
		</style>
	</head>
	<body>
		<div style="width: 10%; float: left;"><img src="images/icono-pelicula.gif"></div>
		<div style="width: 90%; float: right; height: 120px;"><h1 class="titulo">Detalle de ...</h1></div>
		
		<div class="clear"></div>
		<%@ include file="includes/menu.jsp" %>
		
		<div style="margin: 0 auto; margin-top:15px; width: 900px;" class="well bs-component">
		
			<div>
				<div class="btn-group">
			     	<a class="btn btn-info" id="editar">Editar</a>
			     </div>
			    
			    <div class="btn-group">
			     	<a class="btn btn-warning" id="save">Borrar</a>
	     		</div>
			</div>
			
		
			<div class="panel panel-info">
				<div class="panel-heading"><h3 class="panel-title">Clave</h3></div>
				<div id="clave" class="panel-body"></div>
			</div>
			
			<div class="panel panel-info">
				<div class="panel-heading"><h3 class="panel-title">Titulo</h3></div>
				<div id="titulo" class="panel-body"></div>
			</div>
			
            <div class="panel panel-info">
				<div class="panel-heading"><h3 class="panel-title">Productora</h3></div>
            	<div id="productora" class="panel-body"></div>
            </div>
            
            <div class="panel panel-info">
				<div class="panel-heading"><h3 class="panel-title">Categoria</h3></div>
            	<div id="categoria" class="panel-body"></div>
            </div>
            
             <div class="panel panel-info">
				<div class="panel-heading"><h3 class="panel-title">Formato</h3></div>
            	<div id="formato" class="panel-body"></div>
            </div>
            
             <div class="panel panel-info">
				<div class="panel-heading"><h3 class="panel-title">Genero</h3></div>
            	<div id="genero" class="panel-body"></div>
            </div>
            
             <div class="panel panel-info">
				<div class="panel-heading"><h3 class="panel-title">Estado</h3></div>
            	<div id="estado" class="panel-body"></div>
            </div>
            
            <div class="panel panel-info">
				<div class="panel-heading"><h3 class="panel-title">Sipnosis</h3></div>
            	<div id="sipnosis" class="panel-body"></div>
            </div>
            
             <div class="panel panel-info">
				<div class="panel-heading"><h3 class="panel-title">Temporada</h3></div>
            	<div id="temporada" class="panel-body"></div>
            </div>
		</div>
		
		
		<footer>
			Hola este es un pie de prueba
		</footer>
		<script>
			$(document).ready(function(){
				var id = $.urlParam('id');
				cargarPelicula(id, false);
				
				$("#editar").click(function(){
					var url = "editar.jsp?id=" + id;    
					$(location).attr('href',url);
				});
			});
			
			function cargarElemento(peli){
				$('#clave').html(peli.clave);
				$('#titulo').html(peli.titulo);
				$('#productora').html(peli.productora);
				$('#temporada').html(peli.temporada);
				$('#sipnosis').html(peli.sipnosis);
				$('#categoria').html(peli.tipo);
				$('#formato').html(peli.formato);
				$('#estado').html(peli.estado);
				$('#genero').html(peli.genero);
				$('.titulo').html('Detalle de... ' + peli.titulo);
			}
			
		</script>
	</body>
</html>