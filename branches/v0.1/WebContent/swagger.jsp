<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Swagger Api</title>

	<link href="css1/highlight.default.css" rel="stylesheet" type="text/css" />
	<link href="css1/screen.css" rel="stylesheet" type="text/css" />
  	<script src="lib/shred.bundle.js"></script>
  	<script src='lib/jquery-1.8.0.min.js'></script>
  	<script src='lib/jquery.slideto.min.js'></script>
  	<script src='lib/jquery.wiggle.min.js'></script>
  	<script src='lib/jquery.ba-bbq.min.js'></script>
  	<script src='lib/handlebars-1.0.0.js'></script>
  	<script src='lib/underscore-min.js'></script>
  	<script src='lib/backbone-min.js'></script>
  	<script src='lib/swagger.js'></script>
  	<script src='swagger-ui.js'></script>
  	<script src='lib/highlight.7.3.pack.js'></script>
</head>
<body>
	<div id="message-bar" class="swagger-ui-wrap">
	  &nbsp;
	</div>

	<div id="swagger-ui-container" class="swagger-ui-wrap"></div>
	<script>
	    $(function () {
	      window.swaggerUi = new SwaggerUi({
	      url: "http://localhost:8080/PeliculasySeries/server/api-docs",
	      dom_id: "swagger-ui-container",
	      supportedSubmitMethods: ['get', 'post', 'put', 'delete'],
	      onComplete: function(swaggerApi, swaggerUi){
	        log("Loaded SwaggerUI");
	        $('pre code').each(function(i, e) {hljs.highlightBlock(e)});
	      },
	      onFailure: function(data) {
	        log("Unable to Load SwaggerUI");
	      },
	      docExpansion: "none"
	    });
	/*
	    $('#input_apiKey').change(function() {
	      var key = $('#input_apiKey')[0].value;
	      log("key: " + key);
	      if(key && key.trim() != "") {
	        log("added key " + key);
	        window.authorizations.add("key", new ApiKeyAuthorization("api_key", key, "query"));
	      }
	    })*/
	    window.swaggerUi.load();
	  });
	
	  </script>
</body>
</html>