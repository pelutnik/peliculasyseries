<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
    <%@ page isErrorPage="true" import="java.io.*"%>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf8">
		<title>Error 500</title>
		<%@ include file="../includes/head.jsp" %>
		<script>
			$(document).ready(function(){
				$.material.init();
			})
		</script>
	</head>
	<body>
	<h1>Error 500</h1>
	<%@ include file="../includes/menu.jsp" %>
		<div class="well bs-component">
			<h2>Lo siento, el recurso solicitado ha encontrado un error en el servidor</h2>
		
		<p>Exception Message:</p>
   				 <p><% //exception.printStackTrace(response.getWriter()); %></p>

   				<!--  <p>StackTrace:</p>
   				 <p>
   					 <%
   					 /*
   							 StringWriter stringWriter = new StringWriter();
   							 PrintWriter printWriter = new PrintWriter(stringWriter);
   							 exception.printStackTrace(printWriter);
   							 out.println(stringWriter);
   							 printWriter.close();
   							 stringWriter.close();*/
   						 %>
   				 </p>-->
   				 </div>
	</body>
</html>