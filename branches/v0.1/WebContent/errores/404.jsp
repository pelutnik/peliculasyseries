<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf8">
		<%@ include file="../includes/head.jsp" %>
		<title>Error 404</title>
		<script>
			$(document).ready(function(){
				$.material.init();
			})
		</script>
	</head>
	<body>
		<h1>Error 404</h1>
		<%@ include file="../includes/menu.jsp" %>
		<div class="well bs-component">
			<h2>Lo siento, el recurso solicitado no esta disponible</h2>
		</div>
	</body>
</html>