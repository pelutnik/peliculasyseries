<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf8">
		<title>Listado de peliculas y series</title>
		
		
		<%@ include file="includes/head.jsp" %>
		<script src="js/jquery.autocomplete.js"></script>
		
		<!-- A partir de aqui van mis funciones de script -->
		<script src="js/MisScripts/cargarlistado.js"></script>
		<!-- <script src="js/bootstrap-breadcrumb.js"></script> 
		<link href="css/jquery.ui.autocomplete.css" rel="stylesheet" />-->
		<script src="jsMaterial/materialPreloader.js"></script> 
		<script>	
			$(document).ready(function(){
				$.material.init();
				cargarListado();
				$('#mi-tabla').DataTable();

				$('#buscar').autocomplete({
				    //lookup: currencies,
				    serviceUrl:'server/pelisseries/buscar',
				    onSelect: function (suggestion) {
				      var thehtml = '<strong>Currency Name:</strong> ' + suggestion.
				      value + ' <br> <strong>Symbol:</strong> ' + suggestion.data;
				      $('#outputcontent').html(thehtml);
				    }
				  });
			});
			
		</script>
		
	</head>
	<body>	
	
		<!-- <img src="images/camara-de-rollo-de-pelicula-icono-de-psd_30-2217.jpg" style="width: 300px; height: 300px;">-->
		<h1>Listado de peliculas y series</h1>
		<!-- Menu  -->
		<%@ include file="includes/menu.jsp" %>
		<div class="my-table well bs-component">
			<!-- <div id="the-basics">
				<input class="typeahead" id="buscar" type="text" placeholder="Buscar"  data-source="server/pelisseries/buscar?term="  />
				
			</div>
			<div id="outputcontent"></div> -->
			<table class="display" id="mi-tabla">
				<thead>
					<tr>
						<th>#</th>
						<th>Clave</th>
						<th>Titulo</th>
						<th>Categoria</th>
						<th>Formato</th>
						<th>Genero</th>
						<th>Productora</th>
						<th>Temporada</th>
						<th>Estado</th>
						<th>Borrar</th>
					</tr>
				</thead>
				<tbody id="cuerpo-tabla">
				</tbody>
				<tfoot>
					 <tr>
					 	<th>#</th>
						<th>Clave</th>
						<th>Titulo</th>
						<th>Categoria</th>
						<th>Formato</th>
						<th>Genero</th>
						<th>Productora</th>
						<th>Temporada</th>
						<th>Estado</th>
						<th>Borrar</th>
					</tr>
				</tfoot>
			</table>
		</div>	
	</body>
</html>