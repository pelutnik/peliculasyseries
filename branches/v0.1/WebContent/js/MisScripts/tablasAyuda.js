function crearTabla(datos){
	var tabla = '';
	tabla +='<link rel="stylesheet" href="/PeliculasySeries/css/jquery.dataTables.css">';
	tabla +='<script src="/PeliculasySeries/js/jquery.dataTables.js"></script>';
	tabla +='<div class="well bs-component">';
	tabla +='<table id="tablaProductoras" class="display">';
	tabla +='	<thead>';
	tabla +='		<tr>';
	tabla +='			<th class="text-left">Clave</th>';
	tabla +='			<th class="text-left">Nombre</th>';
	tabla +='		</tr>';
	tabla +='	</thead>';
	tabla +='	<tbody id="cuerpo-tabla">';
	tabla +=		mostrarDatos(datos); 
	tabla +='	</tbody>';
	tabla +='	<tfoot>';
	tabla +='		<tr>';
	tabla +='			<th class="text-left">Clave</th>';
	tabla +='			<th class="text-left">Nombre</th>';
	tabla +='		</tr>';
	tabla +='	</tfoot>';
	tabla +='</table>';
	tabla +='</div>';
	tabla +='$("#tablaProductoras").DataTable()';
}

$("#productoras").click(function(){
    
	var tabla = crearTabla(cargarProductoras());
	alert(tabla);
	    BootstrapDialog.show({
	    	type: BootstrapDialog.TYPE_INFO,
	        title: 'Añadir elementos',
	        message: tabla,
	       // onshow:  $('#tablaProductoras').DataTable(),
	        buttons: [{
	            label: 'Guardar',
	            cssClass: 'btn btn-primary',
	            hotkey: 13, // Enter.
	            action: function(dialogItself) {
	            	dialogItself.close();
	            }
	        },
	        {
	            label: 'Cerrar',
	            cssClass: 'btn btn-warning',
	            hotkey: 13, // Enter.
	            action: function(dialogItself) {
	            	dialogItself.close();
	            }
	        }]
	    });
        	/*$.Dialog({
                overlay:true,
                shadow:true,
                flat: true,
                autoresize:true,
                show:"clip",
                hide:"clip",
                draggable:true,
                autoOpen:true,
                icon:'',
                title:'Añadir un nuevo Formato',
                content:window,
                height: 650,
                width: 900,
                //padding:75,
                onshow: function(_dialog){
                	//var content = form;
                    $.Dialog.title('');
                    $.Dialog.content(window);
                    $.Metro.initInputs();
                }

            });*/
	    	
        	//window=''; 
        	
        	
        	
        /*
        	var tabla = '';

        	for (var i =0; i< datos.length; i++){
		    	clave = datos[i].clave;
		    	tabla += '<tr><td><button class="clave" data-role="button">' + clave + '</button></td>'; 
		    	tabla += '<td>' + datos[i].nombre + '</td>';
		    }
        	$("#cuerpo-tabla").html(tabla);
        	$('#tablaProductoras').DataTable();
		    */

		    $('.clave').on("click",function(){
			    var texto ='';
			    texto = $(this).text();
	            $("#produlist").val(texto);
	            //$.Dialog.close();
	        });

		    $("#dynatable-query-search-tablaProductoras").keypress(function(){
		    	$('.clave').on("click",function(){
	 			    var texto ='';
	 			    texto = $(this).text();
	 	            $("#produlist").val(texto);
	 	            //$.Dialog.close();
	 	        }); 
			});
        });


    	$("#tipos").click(function(){
        	var window ='';
        	window +='<link rel="stylesheet" href="/PeliculasySeries/css/jquery.dataTables.css">';
        	window +='<script src="/PeliculasySeries/js/jquery.dataTables.js"></script>';
        	window +='<table id="tablaTipos" class="table striped hovered dataTable table-bordered">';
        	window +='<thead>';
        	window +='<tr>';
        	window +='<th class="text-left">Clave</th>';
        	window +='<th class="text-left">Nombre</th>';
        	window +='</tr>';
        	window +='</thead>';
        	window +='<tbody id="cuerpo-tabla">';
        	window +='</tbody>';
        	window +='<tfoot>';
        	window +='<tr>';
        	window +='<th class="text-left">Clave</th>';
        	window +='<th class="text-left">Nombre</th>';
        	window +='</tr>';
        	window +='</tfoot>';
        	window +='</table>';

        	$.Dialog({
                overlay:true,
                shadow:true,
                flat: true,
                autoresize:true,
                show:"clip",
                hide:"clip",
                draggable:true,
                autoOpen:true,
                icon:'',
                title:'Añadir un nuevo Formato',
                content:window,
                height: 650,
                width: 900,
                //padding:75,
                onshow: function(_dialog){
                	//var content = form;
                    $.Dialog.title('');
                    $.Dialog.content(window);
                    $.Metro.initInputs();
                }

            });
        	window=''; 
        	
        	
        	
        	var datos = cargarTipos();
        	var tabla = '';

        	for (var i =0; i< datos.length; i++){
		    	clave = datos[i].clave;
		    	tabla += '<tr><td><button class="clave" data-role="button">' + clave + '</button></td>'; 
		    	tabla += '<td>' + datos[i].nombre + '</td>';
		    }
        	$("#cuerpo-tabla").html(tabla);
        	//$('#tablaTipos').dynatable();
		    

		    $('.clave').on("click",function(){
			    var texto ='';
			    texto = $(this).text();
	            $("#tipolist").val(texto);
	            $.Dialog.close();
	        });

		    $("#dynatable-query-search-tablaTipos").keypress(function(){
		    	$('.clave').on("click",function(){
	 			    var texto ='';
	 			    texto = $(this).text();
	 	            $("#tipolist").val(texto);
	 	            $.Dialog.close();
	 	        }); 
			});
        });
        
    	$("#formatos").click(function(){
        	var window ='';
        	//window +='<link rel="stylesheet" href="/PeliculasySeries/css/jquery.dataTables.css">';
        	//window +='<script src="/PeliculasySeries/js/jquery.dataTables.js"></script>';
        	window += '<table id="tablaFormatos" class="display dataTable" data-toggle="table">';
        	window +='<thead>';
        	window +='<tr>';
        	window +='<th class="text-left">Clave</th>';
        	window +='<th class="text-left">Nombre</th2q';
        	window +='</tr>';
        	window +='</thead>';
        	window +='<tbody id="cuerpo-tabla">';
        	window += mostrarDatos(cargarFormatos());
        	window +='</tbody>';
        	window +='<tfoot>';
        	window +='<tr>';
        	window +='<th class="text-left">Clave</th>';
        	window +='<th class="text-left">Nombre</th>';
        	window +='</tr>';
        	window +='</tfoot>';
        	window +='</table>';
        
        	
        	
        	BootstrapDialog.show({
        		size: BootstrapDialog.SIZE_WIDE,
    	    	//type: BootstrapDialog.TYPE_INFO,
        		onshow: function() {
        			$("#tablaFormatos").DataTable()
        		},
    	        title: 'Añadir elementos',
    	        cssClass: '<link rel="stylesheet" href="/PeliculasySeries/css/jquery.dataTables.css">',
    	        message: window,
    	        buttons: [{
    	            label: 'Guardar',
    	            cssClass: 'btn btn-primary',
    	            hotkey: 13, // Enter.
    	            action: function(dialogItself) {
    	            	dialogItself.close();
    	            }
    	        },
    	        {
    	            label: 'Cerrar',
    	            cssClass: 'btn btn-warning',
    	            hotkey: 13, // Enter.
    	            action: function(dialogItself) {
    	            	dialogItself.close();
    	            }
    	        }]
    	    });
        	//$('#tablaFormatos').DataTable();
        	/*$.Dialog({
                overlay:true,
                shadow:true,
                flat: true,
                autoresize:true,
                show:"clip",
                hide:"clip",
                draggable:true,
                autoOpen:true,
                icon:'',
                title:'Añadir un nuevo Formato',
                content:window,
                height: 650,
                width: 900,
                //padding:75,
                onshow: function(_dialog){
                	//var content = form;
                    $.Dialog.title('');
                    $.Dialog.content(window);
                    $.Metro.initInputs();
                }*/

            });
        	 
        	
        	window='';
        	
        	

		    $('.clave').on("click",function(){
			    var texto ='';
			    texto = $(this).text();
	            $("#formatlist").val(texto);
	            //$.Dialog.close();
	        });

		    $("#dynatable-query-search-tablaFormatos").keypress(function(){
		    	$('.clave').on("click",function(){
	 			    var texto ='';
	 			    texto = $(this).text();
	 	            $("#formatlist").val(texto);
	 	            //$.Dialog.close();
	 	        }); 
			});
        //});
        

    	function mostrarDatos(datos){
        	
        	var tabla = '';
        	
        	for (var i =0; i< datos.length; i++){
		    	clave = datos[i].clave;
		    	tabla += '<tr><td><a class="btn-link clave">' + clave + '</a></td>'; 
		    	tabla += '<td>' + datos[i].nombre + '</td>';
		    }
        	return tabla;
        	//$("#cuerpo-tabla").html(tabla);
        	
    	}
    	
    	$("#estado").click(function(){
    		var window ='';
    	    window +='<table id="tablaEstados" class="table striped hovered dataTable table-bordered">';
    	    window +='	<thead>';
    	    window +='		<tr>';
    	    window +='			<th class="text-left">Clave</th>';
    	    window +='			<th class="text-left">Nombre</th>';
    	    window +='		</tr>';
    	    window +='	</thead>';
    	    window +='	<tbody id="cuerpo-tabla">';
    	    window +='	</tbody>';
    	    window +='	<tfoot>';
    	    window +='		<tr>';
    	    window +='			<th class="text-left">Clave</th>';
    	    window +='			<th class="text-left">Nombre</th>';
    	    window +='		</tr>';
    	    window +='	</tfoot>';
    	    window +='</table>';

    	        	$.Dialog({
    	                overlay:true,
    	                shadow:true,
    	                flat: true,
    	                autoresize:true,
    	                show:"clip",
    	                hide:"clip",
    	                draggable:true,
    	                autoOpen:true,
    	                icon:'',
    	                title:'Añadir un nuevo Formato',
    	                content:window,
    	                height: 650,
    	                width: 900,
    	                //padding:75,
    	                onshow: function(_dialog){
    	                	//var content = form;
    	                    $.Dialog.title('');
    	                    $.Dialog.content(window);
    	                    $.Metro.initInputs();
    	                }

    	            });
    	        	window=''; 
    	        	
    	        	
    	        	
    	        	var datos = cargarEstados();
    	        	var tabla = '';

    	        	for (var i =0; i< datos.length; i++){
    			    	clave = datos[i].clave;
    			    	tabla += '<tr><td><button class="clave" data-role="button">' + clave + '</button></td>'; 
    			    	tabla += '<td>' + datos[i].nombre + '</td>';
    			    }
    	        	$("#cuerpo-tabla").html(tabla);
    	        	//$('#tablaEstados').dynatable();
    			    

    			    $('.clave').on("click",function(){
    				    var texto ='';
    				    texto = $(this).text();
    		            $("#estadoslist").val(texto);
    		            $.Dialog.close();
    		        });

    			    $("#dynatable-query-search-tablaEstados").keypress(function(){
    			    	$('.clave').on("click",function(){
    		 			    var texto ='';
    		 			    texto = $(this).text();
    		 	            $("#estadoslist").val(texto);
    		 	            $.Dialog.close();
    		 	        }); 
    				});
    	        });
    	
    	$("#genero").click(function(){
    		var window ='';
    	    window +='<table id="tablaGeneros" class="table striped hovered dataTable table-bordered">';
    	    window +='	<thead>';
    	    window +='		<tr>';
    	    window +='			<th class="text-left">Clave</th>';
    	    window +='			<th class="text-left">Nombre</th>';
    	    window +='		</tr>';
    	    window +='	</thead>';
    	    window +='	<tbody id="cuerpo-tabla">';
    	    window +='	</tbody>';
    	    window +='	<tfoot>';
    	    window +='		<tr>';
    	    window +='			<th class="text-left">Clave</th>';
    	    window +='			<th class="text-left">Nombre</th>';
    	    window +='		</tr>';
    	    window +='	</tfoot>';
    	    window +='</table>';

    	        	$.Dialog({
    	                overlay:true,
    	                shadow:true,
    	                flat: true,
    	                autoresize:true,
    	                show:"clip",
    	                hide:"clip",
    	                draggable:true,
    	                autoOpen:true,
    	                icon:'',
    	                title:'Añadir un nuevo Formato',
    	                content:window,
    	                height: 650,
    	                width: 900,
    	                //padding:75,
    	                onshow: function(_dialog){
    	                	//var content = form;
    	                    $.Dialog.title('');
    	                    $.Dialog.content(window);
    	                    $.Metro.initInputs();
    	                }

    	            });
    	        	window=''; 
    	        	
    	        	
    	        	
    	        	var datos = cargarGeneros();
    	        	var tabla = '';

    	        	for (var i =0; i< datos.length; i++){
    			    	clave = datos[i].clave;
    			    	tabla += '<tr><td><button class="clave" data-role="button">' + clave + '</button></td>'; 
    			    	tabla += '<td>' + datos[i].nombre + '</td>';
    			    }
    	        	$("#cuerpo-tabla").html(tabla);
    	        	//$('#tablaGeneros').dynatable();
    			    

    			    $('.clave').on("click",function(){
    				    var texto ='';
    				    texto = $(this).text();
    		            $("#generoslist").val(texto);
    		            $.Dialog.close();
    		        });

    			    $("#dynatable-query-search-tablaGeneros").keypress(function(){
    			    	$('.clave').on("click",function(){
    		 			    var texto ='';
    		 			    texto = $(this).text();
    		 	            $("#generoslist").val(texto);
    		 	            $.Dialog.close();
    		 	        }); 
    				});
    	        });