
function cargarListado(){
	var url = 'server/pelisseries';
	
	$.ajax({
					
		url			:	url,
		dataType	:	'json', 
		type		:	'get',
		async		:	false,
		before		:
			// material design preloader
			preloader = new $.materialPreloader({
		        position: 'top',
		        height: '5px',
		        col_1: '#159756',
		        col_2: '#da4733',
		        col_3: '#3b78e7',
		        col_4: '#fdba2c',
		        fadeIn: 200,
		        fadeOut: 200
		    }),
	
		success		:	function(data){
			cagarTabla(data);
			generarNotificaciones('info','Registros cargados correctamente');
		},
		error 		: 	function(xhr, status, error) {
			//console.error("Error en la petición AJAX de los datos de matrículas: ", error);
			generarNotificaciones('error',data);
		}
	});
}


function cagarTabla(datos){
	var tabla='';
    for (var i =0; i< datos.length; i++){
    	tabla += '<tr><td>' + datos[i].id + '</td>' 
    	tabla += '<td><a href="detalles.jsp?id='+ datos[i].id +'">' + datos[i].clave + '</a></td>'; 
    	tabla += '<td>' + datos[i].titulo + '</td>';
    	tabla += '<td>' + datos[i].tipo + '</td>';
    	tabla += '<td>' + datos[i].formato +'</td>';
    	tabla += '<td>' + datos[i].genero +'</td>';
    	tabla += '<td>' + datos[i].productora + '</td>';
    	if (datos[i].temporada > 0) {
    		tabla += '<td>'+datos[i].temporada + '</td>';	
    	} else{
    		tabla += '<td>0</td>';
    	}
    	tabla += '<td>' + datos[i].estado + '</td>';
    	tabla += '<td><button class="borrar btn-link"><img src="/PeliculasySeries/images/delete.gif" /></button></td></tr>';
    			
    }
    $("#cuerpo-tabla").html(tabla);
    //$('#mi-tabla').dynatable();
}