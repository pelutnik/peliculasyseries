function generarNotificaciones(tipo,mensaje){
	var stack_bar_top = {"dir1": "down", "dir2": "right", "push": "top", "spacing1": 0, "spacing2": 0};
	var opts = {
            title: "Over Here",
            text: mensaje,
            addclass: "stack-bar-top",
            cornerclass: "",
            width: "100%",
            stack: stack_bar_top
			//animation: 'fade',
			
    };
	switch (tipo) {
	    case 'error':
	        opts.title = "Error";
	        opts.text = mensaje;
	        opts.type = "error";
	        break;
	    case 'info':
			//opts.icon = 'Informacion del sistema',
	        opts.title ='Informacion del sistema' ;
	        opts.text = mensaje;
	        opts.type = "info";
	        break;
	    case 'success':
	        opts.title = "Felicitaciones";
	        opts.text = mensaje;
	        opts.type = "success";
	        break;
    }
	new PNotify(opts); 
	/*
	var n= noty({
		layout			:	'top',
		type			: 	tipo,
		theme			:	'defaultTheme',
		text			:	mensaje,
		dismessQueue	:	true,
		animation		:	{
						open		:	{height	:	'toggle'},
						close		:	{height	:	'toggle'},
						easing		:	'swing',
						speed		:	500
		},
		timeout			:	[10000],
		force			:	false,
		modal			:	false,
		maxVisible		:	10,
		killer			:	false,
		closeWith		:	['click'],
	    callback		:	{
	        			onShow		: function() {},
	        			afterShow	: function() {},
	        			onClose		: function() {},
	        			afterClose	: function() {}
	    },
	    buttons: false
	});*/
}