function guardar (url, datos){
	var res = true;
	// Guardar el json en la BD
	$.ajax({
		url			:	url,
		contentType	:	'application/json; charset=utf-8',
		dataType	:	'json',
		type		:	'post',
		data		:	datos,
		async		:	false,
		success		:	function(data){
			generarNotificaciones('success','Elemento guardado correctamente');
			//$.Dialog.close();
		},
		error 		: 	function(xhr, status, error) {
			console.error("Error en la petición AJAX de los datos de matrículas: ", error);
			generarNotificaciones('error','Elemento guardado correctamente');
			res = false;
		}
	});
	return res;
};

function  actualizar (url, datos){
	var res = true;
	
	$.ajax({
		url			:	url,
		contentType	:	'application/json; charset=utf-8',
		dataType	:	'json',
		type		:	'put',
		data		:	datos,
		async		:	false,
		success		:	function(data){
			generarNotificaciones('success','Elemento guardado correctamente');
			//$.Dialog.close();
		},
		error 		: 	function(xhr, status, error) {
			console.error("Error en la petición AJAX de los datos de matrículas: ", error);
			generarNotificaciones('error','Elemento guardado correctamente');
			res = false;
		}
	});
	
	return res;
};

function borrar(url) {
	var res = true;
	
	$.ajax({
		url			:	url,
		//contentType	:	'application/json; charset=utf-8',
		dataType	:	'json',
		type		:	'delete',
		//data		:	datos,
		async		:	true,
		success		:	function(data){
			generarNotificaciones('success','Elemento borrado correctamente');
			//$.Dialog.close();
		},
		error 		: 	function(xhr, status, error) {
			console.error("Error en la petición AJAX de los datos de matrículas: ", error);
			generarNotificaciones('error','Elemento guardado correctamente');
			res = false;
		}
	});
	
	return res;
}