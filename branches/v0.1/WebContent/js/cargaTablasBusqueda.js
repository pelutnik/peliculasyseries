/**
    		aqui va la carga de las productoras en una tabla
    	*/
    	$("#productoras").click(function(){
        	var window ='';
        	window +='<table id="tablaProductoras" class="table striped hovered dataTable table-bordered">';
        	window +='<thead>';
        	window +='<tr>';
        	window +='<th class="text-left">Clave</th>';
        	window +='<th class="text-left">Nombre</th>';
        	window +='</tr>';
        	window +='</thead>';
        	window +='<tbody id="cuerpo-tabla">';
        	window +='</tbody>';
        	window +='<tfoot>';
        	window +='<tr>';
        	window +='<th class="text-left">Clave</th>';
        	window +='<th class="text-left">Nombre</th>';
        	window +='</tr>';
        	window +='</tfoot>';
        	window +='</table>';

        	$.Dialog({
                overlay:true,
                shadow:true,
                flat: true,
                autoresize:true,
                show:"clip",
                hide:"clip",
                draggable:true,
                autoOpen:true,
                icon:'',
                title:'Añadir un nuevo Formato',
                content:window,
                height: 650,
                width: 900,
                //padding:75,
                onshow: function(_dialog){
                	//var content = form;
                    $.Dialog.title('');
                    $.Dialog.content(window);
                    $.Metro.initInputs();
                }

            });
        	window=''; 
        	
        	
        	
        	var datos = cargarProductoras();
        	var tabla = '';

        	for (var i =0; i< datos.length; i++){
		    	clave = datos[i].clave;
		    	tabla += '<tr><td><button class="clave" data-role="button">' + clave + '</button></td>'; 
		    	tabla += '<td>' + datos[i].nombre + '</td>';
		    }
        	$("#cuerpo-tabla").html(tabla);
        	$('#tablaProductoras').dynatable();
		    

		    $('.clave').on("click",function(){
			    var texto ='';
			    texto = $(this).text();
	            $("#produlist").val(texto);
	            $.Dialog.close();
	        });

		    $("#dynatable-query-search-tablaProductoras").keypress(function(){
		    	$('.clave').on("click",function(){
	 			    var texto ='';
	 			    texto = $(this).text();
	 	            $("#produlist").val(texto);
	 	            $.Dialog.close();
	 	        }); 
			});
        });