function cargarFormatos(){
	var url = 'server/formatos';
	var formatos='';
	$.ajax({
		url			:	url,
		dataType	:	'json',
		type		:	'get',
		async		:	false,
		success		:	function(data){
			formatos = data;
			generarNotificaciones('info','Formatos cargados correctamente');
		},
		error 		: 	function(xhr, status, error) {
			console.error("Error en la petición AJAX de los datos de matrículas: ", error);
			generarNotificaciones('error','Error al cargar los formatos de peliculas');
		}
	});
	return formatos;
}

function cargarEstados(){
    var url = 'server/estados';
    var estados='';
    $.ajax({
        	url			:	url,
	        dataType	:	'json', 
	        //contentType	:	'application/json',
         	type		:	'get',
	        //data		:	'data',
	        async		:	false,
	        success		:	function(data){
                //recuperarElementos(data,"formatlist");
                estados = data;
                generarNotificaciones('info','Estados cargados correctamente');
	        },
            error 		: 	function(xhr, status, error) {
                console.error("Error en la petición AJAX de los datos de matrículas: ", error);
                generarNotificaciones('error','Error al cargar los formatos de peliculas');
			//request.abort();
		   }
	    });
    	return estados;
    }   

function cargarGeneros(){
    var url = 'server/generos';
    var estados='';
    $.ajax({
        	url			:	url,
	        dataType	:	'json', 
	        //contentType	:	'application/json',
         	type		:	'get',
	        //data		:	'data',
	        async		:	false,
	        success		:	function(data){
                //recuperarElementos(data,"formatlist");
                estados = data;
                generarNotificaciones('info','Generos cargados correctamente');
	        },
            error 		: 	function(xhr, status, error) {
                console.error("Error en la petición AJAX de los datos de matrículas: ", error);
                generarNotificaciones('error','Error al cargar los formatos de peliculas');
			//request.abort();
		   }
	    });
    	return estados;
    }

function cargarTipos(){
    var url = 'server/tipospeliculas';
    var tipos='';
    $.ajax({
    	url			:	url,
    	dataType	:	'json', 
    	type		:	'get',
    	async		:	false,
    	success		:	function(data){
    		tipos = data;
    		generarNotificaciones('info','Tipos cargados correctamente');
	    },
	    error 		: 	function(xhr, status, error) {
	    	console.error("Error en la petición AJAX de los datos de matrículas: ", error);
	    	generarNotificaciones('error','Error al cargar los tipos de peliculas');
	    }
	});
    return tipos;
}



function cargarProductoras(){
    var url = 'server/productoras';
    var productoras;
    $.ajax({
    	url			:	url,
    	dataType	:	'json',
    	type		:	'get',
    	async		:	false,
    	success		:	function(data){
    		productoras = data;
    		generarNotificaciones('info','Productoras cargadas correctamente');               
	    },
	    error 		: 	function(xhr, status, error) {
	    	console.error("Error en la petición AJAX de los datos de matrículas: ", error);
	    	generarNotificaciones('error','Error al cargar las productoras de peliculas');
		}
	});
	return productoras;
}

function recuperarElementos(data, idElemento){
    var opcionesclave='';
    var opcionesnombre='';
    var opciones='<option value="">Seleccionar un elemento</option>';

    $("#"+ idElemento).children().remove();
    
    for (var i =0; i < data.length;i++){
        opciones +='<option value='+ data[i].clave + '>' + data[i].nombre + '</option>' 
    }
    $("#"+ idElemento).append(opciones);
}


function getFormData($form){
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}

function cargarPelicula(id, edicion){

	var url = 'server/pelisseries/peliculaunica?id=' + id + '&edicion=' + edicion;
	
	
	$.ajax({
					
		url			:	url,
		dataType	:	'json', 
		type		:	'get',
		async		:	true,
		success		:	function(data){
			cargarElemento(data);
			generarNotificaciones('info','Registro cargado correctamente');
		},
		error 		: 	function(xhr, status, error) {
			//console.error("Error en la petición AJAX de los datos de matrículas: ", error);
			generarNotificaciones('error',xhr);
		}
	});
}

$.urlParam = function(name){
	 var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	    if (results==null){
	       return null;
	    }
	    else{
	       return results[1] || 0;
	    }
}
