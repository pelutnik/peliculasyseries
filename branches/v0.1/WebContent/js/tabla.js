var urljson = 'server/pelisseries';

$(function(){
	$('#dataTables-1').dataTable( {
		"bProcessing": true,
        "sAjaxSource": urljson,//"dataTables-objects.txt",
        "aoColumns": [
                      { "mData": "clave" },
                      { "mData": "titulo" },
                      { "mData": "tipo" },
                      { "mData": "formato" },
                      { "mData": "productora" },
                      { "mData": "temporada" }
                      ]
          } );
});