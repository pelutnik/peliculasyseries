<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf8">
	<title>Listado total de peliculas y series</title>
	<%@ include file="../includes/head.jsp" %>
	
	 
	<link href="/PeliculasySeries/css/style.css" rel="stylesheet" />
	<script src="/PeliculasySeries/js/pdfobject.js"></script>
	
</head>
<body>
	<h1>Listado general de las peliculas</h1>
	<!-- Menu  -->
	<%@ include file="../includes/menu.jsp" %>
	
	
	
	<!--<div id="pdf"><a id="the-canvas">Informe general</a></div>-->
	<div id="viewer" class="pdf-viewer" data-url=""></div>
 	
	<script>
	var url = '../server/estados/informe';
	//var urlInforme ='';
	
	$(document).ready(function(){
		$.ajax({
			url			:	url,
			type		: 	'get',
			async		:	false,
			success		:	function(data){
				$('#viewer').attr('data-url',data.mensaje);
				viewer = new PDFViewer($('#viewer'));
			},
			error 		: 	function(xhr, status, error) {
				console.error("Error en la petición AJAX de los datos de matrículas: ", error);
			}
		})
	})      
	</script>
	
</body>
</html>