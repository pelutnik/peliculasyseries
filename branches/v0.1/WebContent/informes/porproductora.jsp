<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf8">
	<title>Listado de peliculas o series agrupadas por productora</title>
	<%@ include file="../includes/head.jsp" %>
	<!-- <script src="../js/jquery-2.1.0.min.js"></script>
	
	<script src="../js/pdfobject.js"></script>
		-->
		<script src="../js/pdfobject.js"></script>
		<link href="../css/style.css" rel="stylesheet" />
</head>
<body>
	<h1>Listado de peliculas y series agrupadas por productora</h1>
	<!-- Menu  -->
	<%@ include file="../includes/menu.jsp" %>
	
	<div id="viewer" class="pdf-viewer" data-url=""></div>
	
	<script>
		var url = '../server/productoras/informe';
		//var urlInforme ='';
		
		$(document).ready(function(){
			$.ajax({
				url			:	url,
				type		: 	'get',
				async		:	false,
				success		:	function(data){
					$('#viewer').attr('data-url',data.mensaje);
					viewer = new PDFViewer($('#viewer'));
				},
				error 		: 	function(xhr, status, error) {
					console.error("Error en la petición AJAX de los datos de matrículas: ", error);
					//request.abort();
				}
			});
		});
      
	</script>
	
</body>
</html>