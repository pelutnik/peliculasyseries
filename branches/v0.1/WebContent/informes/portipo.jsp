<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf8">
		<title>Informe de peliculas y series agrupados por tipo</title>
		<%@ include file="../includes/head.jsp" %>
		
		<link href="/PeliculasySeries/css/style.css" rel="stylesheet" />
		 <script src="/PeliculasySeries/js/pdfobject.js"></script>
		
	</head>
	<body>
		<h1>Informe por tipos de peliculas y series</h1>
		<!-- Menu  -->
		<%@ include file="../includes/menu.jsp" %>
		
		<div id="viewer" class="pdf-viewer" data-url=""></div>
	 	
		<script>
		var url = '../server/tipospeliculas/informe';
		//var urlInforme ='';
		
		$(document).ready(function(){
			$.ajax({
				url			:	url,
				type		: 	'get',
				async		:	false,
				success		:	function(data){
					$('#viewer').attr('data-url',data.mensaje);
					viewer = new PDFViewer($('#viewer'));
				},
				error 		: 	function(xhr, status, error) {
					console.error("Error en la petición AJAX de los datos de matrículas: ", error);
					//request.abort();
				}
			})
		});
		
	
	
	      
		</script>
		
	</body>
</html>