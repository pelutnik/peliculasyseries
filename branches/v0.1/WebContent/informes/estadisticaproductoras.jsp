<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE>
<html lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Informe por tipo</title>
	<%@ include file="../includes/head.jsp" %>
	<link href="../css/style.css" rel="stylesheet" type="text/css" />
	<script src="../js/pdfobject.js"></script>
	
</head>
<body class="metro">
	<h1>Informe de estadistica por productora de peliculas y series</h1>
	<!-- Menu  -->
	<!-- <%@ include file="../includes/menu.jsp" %> -->
	
	<div id="pdf"><a id="the-canvas">ss</a></div>
 	
	<script>
	var url = '../server/informes/estadistica/porproductora';
	//var urlInforme ='';
	
	$(document).ready(function(){
		$.ajax({
			url			:	url,
			type		: 	'get',
			async		:	false,
			success		:	function(data){
				//$('#viewer').attr('href',data);
				//urlInforme = data;

				cargarPDF(data);
			},
			error 		: 	function(xhr, status, error) {
				console.error("Error en la petición AJAX de los datos de matrículas: ", error);
				//request.abort();
			}
		})
	})
	
	function cargarPDF(urlPDF){
		window.onload = function (){
			var myPDF = new PDFObject({ 
				url: urlPDF,
				pdfOpenParams: {
					//navpanes: 1,
					view: "FitV"//,
					//pagemode: "thumbs"
				}
			}).embed("pdf");
		
		};
		
	}

      
	</script>
	
</body>
</html>