<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf8">
        <title>Nuevo elemento Multimedia</title>
		<%@ include file="includes/head.jsp" %>
		

		 <!--A partir de aqui van mis funciones de script 	-->
		<script src="js/MisScripts/formulariosPequenos.js"></script>
		<script src="js/MisScripts/funcionesguardado.js"></script>
		
    </head>
    <body>
    	<h1>Nuevo contenido</h1>
    	
    	<%@ include file="includes/menu.jsp" %>
    	<h2>Introducir nuevo elemento</h2>
    	<%@ include file="includes/formularioPeliculas.jsp" %>
    	
    	<script>

    		$(document).ready(function(){
    			$.material.init();
    			
	    		recuperarElementos(cargarFormatos(),'formatlist');
	    		recuperarElementos(cargarTipos(),'tipolist');
	    		recuperarElementos(cargarProductoras(),'produlist');
	    		recuperarElementos(cargarEstados(),'estadoslist');
	    		recuperarElementos(cargarGeneros(), 'generoslist');
	    		
	    		/*$('.selectpicker').selectpicker({
					//style: 'btn-info',
					size: 10
				});
	    	    */
    		});

    		$("#new").click(function() {
    	    	$('#formPrincipal')[0].reset();
			});

            /*Crear un JSON para almacenar la pelicula introducida*/
            $("#save").click(function(){
            	var sipnosistext = $("#sipnosis").val();
				 var datos = JSON.stringify(getFormData($('#formPrincipal')));                
                console.log(datos);
                var url = 'server/pelisseries/insertar';

				if 	(guardar(url, datos)){
					$(this).closest('form').find("input[type=text], textarea, select").val("");
				}
            });
    	</script>
    	<!-- <script src="js/MisScripts/tablasAyuda.js"></script>-->
    	<!-- <script>	
			$(document).ready(function(){
				
			});
		</script>-->
    </body>
</html>