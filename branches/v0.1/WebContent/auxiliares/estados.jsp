<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf8">
		<title>Listado de estados de peliculas y series</title>
		<%@ include file="../includes/head.jsp" %>
		<!-- <script src="/PeliculasySeries/js/metro.min.js"></script>-->
		<script src="js/funcionescomunes.js"></script>
		<script>	
				$(document).ready(function(){
					var urlCargaListado = '../server/estados';
					cargarlistado(urlCargaListado,"detalleEstado");
					$('.table').DataTable();


					$('.borrar').click(function(){
						var id = obtenerId(this);
						var titulo = obtenerTitulo(this);
						bootbox.confirm('¿Estas seguro de eliminar "' + titulo + '"?', function(result){
		                    if (result==true){
								borrar('../server/estados/borrar?id=' + id);
								$("#cuerpo-tabla").html("");
								cargarlistado(urlCargaListado);
				            };
					});
				});
			});
			</script>	
	</head>
	<body>
		<h1>Listado de estados de peliculas y series</h1>
		<!-- Menu  -->
		<%@ include file="../includes/menu.jsp" %>
		
		<div class="my-table well bs-component">
			<table class="table table-striped table-hover" id="tabla-productoras">
				<thead>
					<tr>
						<th class="text-left">#</th>
						<th class="text-left">Clave</th>
						<th class="text-left">Nombre</th>
						<th class="text-left">Borrar</th>
					</tr>
				</thead>
				<tbody id="cuerpo-tabla">
				</tbody>
				<tfoot>
					<tr>
						<th class="text-left">#</th>
						<th class="text-left">Clave</th>
						<th class="text-left">Nombre</th>
						<th class="text-left">Borrar</th>
					</tr>
				</tfoot>
			</table>
		</div>
	</body>
</html>