<div class="well bs-component" >
	
		<form id="formulario" class="form-horizontal">
			<fieldset>
				<legend id="leyenda">Titulo de la seccion</legend>
					
					
				<div class="form-group">
					<label for="clave" id="lblClave" class="col-lg-2 control-label">Clave</label>
					<div class="col-lg-10">
						<input class="form-control" type="text" id="clave" name="clave" value="" readonly="readonly" />
					</div>
				</div>
				
				<div class="form-group">
					<label for="nombre" id="lblNombre" class="col-lg-2 control-label">Nombre</label>
					<div class="col-lg-10" data-role="input-control">
						<input class="form-control" type="text" id="nombre" name="nombre" value="" readonly="readonly" />
					</div>
				</div>	
				
				<div class="form-group">
					<label for="descripcion" id="lblDescripcion" class="col-lg-2 control-label">Descripcion</label>
					<div class="col-lg-10">
						<input type="text" id="descripcion" name="descripcion" value="" class="form-control" />
					</div>
				</div>
				</fieldset>
			</form>
			<div class="btn-group">
		<a class="btn btn-primary" id="nuevo" href="#">Nuevo</a>
	</div>
	    
	<div class="btn-group">
		<a class="btn btn-primary" id="guardar" href="#">Guardar</a>
	</div>
		</div>