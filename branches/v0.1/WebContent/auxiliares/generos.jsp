<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf8">
		<title>Listado de generos de peliculas y series</title>
		<%@ include file="../includes/head.jsp" %>
		<script src="js/funcionescomunes.js"></script>
		
		<script>	
				$(document).ready(function(){
					var urlCargaListado = '../server/generos';
					cargarlistado(urlCargaListado,"detalleGenero");
					
					$('.table').DataTable();
					
					$('.borrar').click(function(){
						var id = $(this).parent("td").parent("tr").find(".id").html();
						var titulo = $(this).parent("td").parent("tr").find(".titulo").html();
						 bootbox.confirm('¿Estas seguro de eliminar "' + titulo + '"?', function(result){
			                    if (result==true){
									borrar('../server/generos/borrar?id=' + id);
									$("#cuerpo-tabla").html("");
									cargarlistadoGeneros();
					            };
			             });
				    });
				});
			</script>	
	</head>
	<body class="metro">
		<h1>Listado de generos de peliculas y series</h1>
		<!-- Menu  -->
		<%@ include file="../includes/menu.jsp" %>
		<div class="my-table well bs-component">
			<table class="table table-striped table-hover" id="tabla-generos">
				<thead>
					<tr>
						<th class="text-left">#</th>
						<th class="text-left">Clave</th>
						<th class="text-left">Nombre</th>
						<th class="text-left">Borrar</th>
					</tr>
				</thead>
				<tbody id="cuerpo-tabla">
				</tbody>
				<tfoot>
					<tr>
						<th class="text-left">#</th>
						<th class="text-left">Clave</th>
						<th class="text-left">Nombre</th>
						<th class="text-left">Borrar</th>
					</tr>
				</tfoot>
			</table>
		</div>
	</body>
</html>