<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf8">
		<title class="titulo">Detalle de la productora</title>
		<%@ include file="../includes/head.jsp" %>
		<style type="text/css">
			.clear{
				border: 0 none;
			    clear: both;
			    font-size: 0;
			    height: 0;
			}
		</style>
		
		<script>
			$(document).ready(function(){
				var id = $.urlParam('id');
				cargarCategoria(id);
				
				$("#editar").click(function(){
					var url = "editar.jsp?id=" + id +'&seccion=productora';    
					$(location).attr('href',url);
				});

				function cargarCategoria(id){
					var url = '../server/productoras/detalle?id=' + id;
					
					$.ajax({
						url			:	url,
						dataType	:	'json', 
						type		:	'get',
						async		:	true,
						success		:	function(data){
							cargarElemento(data);
							generarNotificaciones('information','Registro cargado correctamente');
						},
						error 		: 	function(xhr, status, error) {
							generarNotificaciones('error',xhr);
						}
					});
				}

				function cargarElemento(productora){
					$('#clave').html(productora.clave);
					$('#nombre').html(productora.nombre);
					$('#descripcion').html(productora.descripcion);
					$('.titulo').html('Detalle de... ' + productora.nombre);
				}
				
			});
		</script>
	</head>
	<body>
		<div style="width: 10%; float: left;"><img src="../images/icono-pelicula.gif"></div>
		<div style="width: 90%; float: right; height: 120px;"><h1 class="titulo">Detalle de ...</h1></div>
		<div class="clear"></div>
		<%@ include file="../includes/menu.jsp" %>
		<%@ include file="includes/paneles.jsp" %>
		
	</body>
</html>