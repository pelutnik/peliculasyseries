function cagarTabla(datos, detalles){
	var tabla='';
	for (var i =0; i< datos.length; i++){
		tabla += '<tr>';
		tabla += '<td class="right"><span class="id">' + datos[i].id + '</span></td>';
		tabla += '<td class="left"><a href="'+ detalles +'.jsp?id='+ datos[i].id +'">' +  datos[i].clave + '</a></td>'; 
		tabla += '<td class="left"><span class="titulo">' + datos[i].nombre + '</span></td>';
		tabla += '<td class="left"><button class="borrar btn-link"><img src="/PeliculasySeries/images/delete.gif" /></button></td>';
		tabla += '</tr>';
	}
	$("#cuerpo-tabla").html(tabla);
}



function cargarlistado(url,detalles){
	
	$.ajax({
					
		url			:	url,
		dataType	:	'json', 
		type		:	'get',
		async		:	false,
		success		:	function(data){
			cagarTabla(data,detalles);
			generarNotificaciones('info','Registros cargados correctamente');
		},
		error 		: 	function(xhr, status, error) {
			//console.error("Error en la petición AJAX de los datos de matrículas: ", error);
			generarNotificaciones('error',xhr);
		}
	});
}

function obtenerId(boton){
	return $(boton).parent("td").parent("tr").find(".id").html();
}

function obtenerTitulo(boton){
	return $(boton).parent("td").parent("tr").find(".titulo").html();
}