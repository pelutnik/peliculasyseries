<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf8">
		<title class="titulo">Insert title here</title>
		<%@ include file="../includes/head.jsp" %>
		<script src="/PeliculasySeries/js/metro.min.js"></script>
				
		<script>
			$(document).ready(function(){
				var id = $.urlParam('id');
				var seccion = $.urlParam('seccion');
				var urlConsulta = '';
				var urlModificacion = '';
				switch (seccion){
					case 'tipos':
						urlConsulta = '/PeliculasySeries/server/tipospeliculas/detalle?id=' + id;
						urlModificacion = '/PeliculasySeries/server/tipospeliculas/actualizar?id=' + id;
						seccion ='categoria';
						break;
					case 'productora':
						urlConsulta = '/PeliculasySeries/server/productoras/detalle?id=' + id;
						urlModificacion = '/PeliculasySeries/server/productoras/actualizar?id=' + id;
						break;
					case 'formatos':
						urlConsulta = '/PeliculasySeries/server/formatos/detalle?id=' + id;
						urlModificacion = '/PeliculasySeries/server/formatos/actualizar?id=' + id;
						break;
					case 'estados':
						urlConsulta = '/PeliculasySeries/server/estados/detalle?id=' + id;
						urlModificacion = '/PeliculasySeries/server/estados/actualizar?id=' + id;
						break;
					case 'generos':
						urlConsulta = '/PeliculasySeries/server/generos/detalle?id=' + id;
						urlModificacion = '/PeliculasySeries/server/generos/actualizar?id=' + id;
						break;
				}
				
				
				$.ajax({
					url			:	urlConsulta,
					dataType	:	'json', 
					type		:	'get',
					async		:	true,
					success		:	function(data){
						$('#clave').val(data.clave);
						$('#nombre').val(data.nombre);
						$('#descripcion').val(data.descripcion);
						$('.titulo').html('Editar elemento ' + data.nombre +' (Seccion "' + seccion + '")');
						generarNotificaciones('information','Registro cargado correctamente');
					},
					error 		: 	function(xhr, status, error) {
						generarNotificaciones('error',xhr);
					}
				});
				$('#guardar').click(function(){
					var datos = JSON.stringify(getFormData($('#formulario')));
					actualizar (urlModificacion, datos);
				});

				$("#nuevo").attr("disabled", "disabled");
			});

			
		</script>
	</head>
	<body class="metro">
		<h1 class="titulo">Edicion del elemento... </h1>
		<!-- Menu  -->
		<%@ include file="../includes/menu.jsp" %>
		<%@ include file="includes/formulario.jsp" %>
	</body>
</html>