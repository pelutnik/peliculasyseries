<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf8">
		<%@ include file="../includes/head.jsp" %>
		<title class="titulo">Insert title here</title>
		<script src="../js/MisScripts/funcionesguardado.js"></script>
	</head>
	<body>
		<h1 class="titulo">Nuevo elemento auxiliar</h1>
		<%@ include file="../includes/menu.jsp" %>
		<%@ include file="includes/formulario.jsp" %>
		
		<script>
			$(document).ready(function(){
				var seccion = $.urlParam('seccion');
				var urlModificacion = '';
				var titulo ='';
				switch (seccion){
					case 'tipo':
						urlModificacion = '../server/tipospeliculas/insertar';
						$('.titulo').html('Nueva Categoria');
						break;
					case 'productora':
						urlModificacion = '../server/productoras/insertar';
						$('.titulo').html('Nueva Productora');
						break;
					case 'formatos':
						urlModificacion = '../server/formatos/insertar';
						$('.titulo').html('Nuevo Formato');
						break;
					case 'estado':
						urlModificacion = '../server/estados/insertar';
						$('.titulo').html('Nuevo Estado');
						break;
					case 'genero':
						urlModificacion = '../server/generos/insertar';
						$('.titulo').html('Nuevo Genero');
						break;
				};
				
				
								
				$('#guardar').click(function(){
					var datos = JSON.stringify(getFormData($('#formulario')));
					guardar(urlModificacion, datos);
				});

				$("#nuevo").click(function() {
			     	   $('#formulario')[0].reset();
			    });

				$("#clave, #nombre").removeAttr("readonly");
			});

			
		</script>
	</body>
</html>