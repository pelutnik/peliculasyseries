SELECT
	peliculas.id as Id, 
	peliculas.clave AS Clave, 
	peliculas.nombre AS Nombre, 
	tipos.nombre AS Tipo, 
	formatos.nombre AS Formato, 
	productoras.nombre AS Productora, 
	peliculas.temporada AS Temporada, 
	estados.nombre as Estado,
	generos.nombre as Genero 
FROM 
	peliculas
	INNER JOIN 
		productoras 
	ON (
		peliculas.productoras = productoras.clave
	)
	INNER JOIN 
		tipos 
	ON (
		peliculas.tipo = tipos.clave
	)
	INNER JOIN 
		formatos 
	ON (
		peliculas.formato = formatos.clave
	)
	INNER JOIN 
		estados
	ON (
		peliculas.estados = estados.clave
	)
	INNER JOIN 
		generos 
	ON (
		peliculas.generos = generos.clave
	)
ORDER BY 
	peliculas.nombre;