SELECT dbo.peliculas.nombre,
	dbo.peliculas.temporada,
	dbo.peliculas.clave,
	dbo.productoras.nombre AS Productora,
	dbo.tipos.nombre AS Tipo,
	dbo.formatos.nombre AS Formato
FROM dbo.peliculas
	 INNER JOIN dbo.formatos ON 
	 dbo.formatos.clave = dbo.peliculas.formato 
	 INNER JOIN dbo.tipos ON 
	 dbo.tipos.clave = dbo.peliculas.tipo 
	 INNER JOIN dbo.productoras ON 
	 dbo.productoras.clave = dbo.peliculas.productoras 
GROUP BY dbo.productoras.clave,
	dbo.peliculas.clave,
	dbo.productoras.nombre,
	dbo.tipos.nombre,
	dbo.formatos.nombre,
	dbo.peliculas.temporada,
	dbo.peliculas.nombre