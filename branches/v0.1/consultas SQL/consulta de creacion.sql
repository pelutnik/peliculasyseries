CREATE DATABASE PeliculasSeries;

USE PeliculasSeries;
/*Creacion de la tabla de los tipos, si es P - pelicula; S - Serie; */
CREATE TABLE tipos (
						id BIGINT NOT NULL IDENTITY(1,1) primary key, 
						clave NVARCHAR(3) NOT NULL DEFAULT '', 
						nombre NVARCHAR(255) NOT NULL DEFAULT '',
						descripcion NVARCHAR(255) DEFAULT ''
					);
/**/
CREATE TABLE formatos (
						id BIGINT NOT NULL IDENTITY(1,1) primary key, 
						clave NVARCHAR(3) NOT NULL DEFAULT '', 
						nombre NVARCHAR(255) NOT NULL DEFAULT '',
						descripcion NVARCHAR(255) DEFAULT ''
					);
/**/
CREATE TABLE estados (
						id BIGINT NOT NULL IDENTITY(1,1) primary key, 
						clave NVARCHAR(3) NOT NULL DEFAULT '', 
						nombre NVARCHAR(255) NOT NULL DEFAULT '',
						descripcion NVARCHAR(255) DEFAULT ''
					);
/**/
CREATE TABLE productoras (
						id BIGINT NOT NULL IDENTITY(1,1) primary key, 
						clave NVARCHAR(5) NOT NULL DEFAULT '', 
						nombre NVARCHAR(255) NOT NULL DEFAULT '',
						descripcion NVARCHAR(255) DEFAULT ''
					);
CREATE TABLE generos (
						id BIGINT NOT NULL IDENTITY(1,1) primary key, 
						clave NVARCHAR(5) NOT NULL DEFAULT '', 
						nombre NVARCHAR(255) NOT NULL DEFAULT '',
						descripcion NVARCHAR(255) DEFAULT ''
					);
/**/
CREATE TABLE peliculas (
						id BIGINT NOT NULL IDENTITY(1,1) primary key, 
						clave NVARCHAR(5) NOT NULL DEFAULT '', 
						nombre NVARCHAR(255) NOT NULL DEFAULT '',
						formato NVARCHAR(3) NOT NULL DEFAULT '',
						tipo NVARCHAR(3) NOT NULL DEFAULT '',
						productoras NVARCHAR(5) NOT NULL DEFAULT '',
						temporada INT DEFAULT 0,
						estados NVARCHAR(3) NOT NULL DEFAULT '',
						sipnosis NTEXT DEFAULT ''
					);